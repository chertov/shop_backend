
set -e
export CGO_ENABLED=0
export GOARCH=amd64
PATH=/usr/local/go/bin/:$PATH
export GOOS=linux
case "$OSTYPE" in
  solaris*) echo "SOLARIS" ;;
  darwin*)  export GOOS=darwin ;; 
  linux*)   export GOOS=linux ;;
  bsd*)     echo "BSD" ;;
  msys*)    echo "WINDOWS" ;;
  *)        echo "unknown: $OSTYPE" ;;
esac
ROOT_PATH=$(pwd)
export GOPATH=$ROOT_PATH:$ROOT_PATH/app
MODULE_PATH=$ROOT_PATH/app/src/app
TARGET_PATH=$ROOT_PATH/bin
PKG_PATH=$ROOT_PATH/pkg
echo GOPATH: $GOPATH
echo ROOT_PATH: $ROOT_PATH
echo MODULE_PATH: $MODULE_PATH
echo PKG_PATH: $PKG_PATH
echo TARGET_PATH: $TARGET_PATH
echo
cd $MODULE_PATH

# go generate app.go

# Скачиваем пакеты,но не собираем
go get -t -d -v ./
echo

go generate app.go

# Собираем для linux 64
export GOOS=linux
TARGET=$TARGET_PATH/server
echo Build for linux...
echo GOOS: $GOOS
echo GOARCH: $GOARCH
echo TARGET: $TARGET
rm -f $TARGET
go build -o $TARGET -i -pkgdir $PKG_PATH
cd $ROOT_PATH
