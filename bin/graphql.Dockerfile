FROM alpine:3.4
MAINTAINER Chertov Maxim <chertovmv@gmail.com>

# for build:
# docker build -t graphql -f graphql.Dockerfile ./

# for run
# docker run -p 8080:8080 -i -t graphql

RUN set -x \
  && apk add --no-cache --virtual .run-deps \
    ca-certificates

RUN mkdir /app
RUN mkdir /app/bin

ADD ./ /app/bin/

WORKDIR /app/bin/
RUN chmod 777 ./server
EXPOSE 8080
ENV GOOGLE_APPLICATION_CREDENTIALS brainfuck-2d73795b7960.json

ENTRYPOINT ./server
