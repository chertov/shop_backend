package gql

import (
	"github.com/mitchellh/mapstructure"
)

type PhoneCheckResult struct {
	Message 	string
	Password 	string
}
func (r *PhoneCheckResult) FromTNT(data interface{}) error {
	err := mapstructure.Decode(data, r)
	if err != nil {
		return err
	}
	return nil
}
