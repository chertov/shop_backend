package gql

import (
	"db/ProductsDB"
	"github.com/neelance/graphql-go"
)

type categoryResolver struct {
	h *ProductsDB.Category
	folder *ProductsDB.Folder
}
func (r categoryResolver) ID() graphql.ID {
	return toGraphID(r.h.BaseFolder.BaseProps.Id)
}
func (r categoryResolver) Parent() graphql.ID {
	if r.folder == nil {
		return graphql.ID("")
	}
	return toGraphID(r.folder.BaseFolder.BaseProps.Id)
}
func (r* categoryResolver) SetParent(folder *ProductsDB.Folder) {
	r.folder = folder
}
func (r categoryResolver) Title() string {
	return r.h.BaseFolder.BaseProps.Title
}
func (r categoryResolver) ShortTitle() string {
	return r.h.BaseFolder.BaseProps.ShortTitle
}
func (r categoryResolver) Summary() string {
	return r.h.BaseFolder.BaseProps.Summary
}
func (r categoryResolver) Description() string {
	return r.h.BaseFolder.BaseProps.Description
}
func (r categoryResolver) ShowOnSite() bool {
	return r.h.BaseFolder.BaseProps.ShowOnSite
}
func (r categoryResolver) Image() graphql.ID {
	return toGraphIDstr(r.h.BaseFolder.Image)
}
func (r categoryResolver) Products() []graphql.ID {
	products := make([]graphql.ID, 0)
	for _, id := range r.h.Products {
		product := db.Products[id]
		if product.ShowOnSite() {
			products = append(products, product.ID())
		}
	}
	return products
}
func (r categoryResolver) Properties() []*propertyResolver {
	properties := make([]*propertyResolver, 0)
	for _, id := range r.h.PropertySet.Properties {
		properties = append(properties, db.Properties[id])
	}
	return properties
}


func (r categoryResolver) SetToChilds() {
	for _, id := range r.h.Products {
		product := db.Products[id]
		product.SetParent(r.h)
	}
}
