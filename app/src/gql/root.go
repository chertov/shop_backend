package gql

import (
	"context"
)

type rootResolver struct {}
func (r *rootResolver) Folders(ctx context.Context) ([]*folderResolver, error) {
	folders := make([]*folderResolver, 0)
	for _, id := range db.Root.Folders {
		folder := db.Folders[id]
		if folder.ShowOnSite() {
			folders = append(folders, folder)
		}
	}
	return folders, nil
}
func (r *rootResolver) Categories(ctx context.Context) ([]*categoryResolver, error) {
	categories := make([]*categoryResolver, 0)
	for _, id := range db.Root.Categories {
		category := db.Categories[id]
		if category.ShowOnSite() {
			categories = append(categories, category)
		}
	}
	return categories, nil
}
func (r *rootResolver) Hello() string {
	return "Hello, World!"
}