package gql

import (
	"context"
	"log"
	"kinds"
	"kvdb"
	"telegrambot"
	"tools"
	"errs"
	"crypto/md5"
	"encoding/hex"
	"regexp"
	"sms"
)

func (r Resolver) Me(ctx context.Context) (*userResolver, error) {
	ses, ok, err := kvdb.GetFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, nil
	}
	if ses.User == nil {
		return nil, nil
	}
	user := kinds.User{}
	err = user.FromTNT(ses.User)
	if err != nil {
		log.Println("Login user.FromTNT error ", err)
		return nil, err
	}

	return &userResolver{&user}, nil
}
func (r Resolver) Login(ctx context.Context, args *struct{Phone string; Password string}) (*userResolver, error) {
	if args == nil {
		return nil, nil
	}
	if !phoneCheck.MatchString(args.Phone) {
		return nil, errs.PHONE_ISNT_VALID.ToFrontend()
	}
	user, err := kvdb.GetUserByPhone(args.Phone)
	if err != nil {
		log.Println("Login kvdb.GetUserByPhone", err)
		return nil, errs.INTERNAL_ERROR.ToFrontend()
	}
	if user == nil {
		log.Println("Login kvdb.GetUserByPhone user == nil")
		return nil, errs.PHONE_NOT_FOUND.ToFrontend()
	}

	user, order, err := kvdb.Login(args.Phone, args.Password)
	if err != nil {
		log.Println("Login kvdb.Login", err)
		return nil, errs.INTERNAL_ERROR.ToFrontend()
	}
	if user == nil {
		log.Println("kvdb.Login user == nil")
		return nil, errs.USER_PASSWORD_INCORRECT.ToFrontend()
	}

	ses, ok, err := kvdb.GetFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, nil
	}

	ses.User = user
	if order != nil {
		if ses.Order != nil {
			sesorder := kinds.Order{}
			sesorder.FromTNT(ses.Order)

			for _, item := range sesorder.Items {
				exists := false
				for _, orditem := range order.Items {
					if orditem.ModelId == item.ModelId {
						exists = true
						break
					}
				}
				if !exists {
					order.Items = append(order.Items, item)
				}
			}
		}
		ses.Order = order
	}
	kvdb.SaveSession(ses)
	return &userResolver{user}, nil
}
func (r Resolver) Logout(ctx context.Context) (*userResolver, error) {
	ses, ok, err := kvdb.GetFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, nil
	}

	ses.User = nil
	ses.Order = nil
	kvdb.SaveSession(ses)
	return nil, nil
}


var phoneCheck = regexp.MustCompile(`^\+7[0-9]{10}$`)
func (r Resolver) NewPassword(ctx context.Context, args *struct{Phone string}) (string, error) {
	ses, ok, err := kvdb.GetFromContext(ctx)
	if err != nil {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}
	if !ok {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}
	if ses.User != nil {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}
	if !phoneCheck.MatchString(args.Phone) {
		return "", errs.PHONE_ISNT_VALID.ToFrontend()
	}

	user, err := kvdb.GetUserByPhone(args.Phone)
	if err != nil {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}
	if user == nil {
		return "", errs.PHONE_NOT_FOUND.ToFrontend()
	}
	password := tools.NewPassword()
	md5sum := md5.Sum([]byte(args.Phone + password))
	var bytes []byte = md5sum[:]
	user.PasswordHash = hex.EncodeToString(bytes)
	err = kvdb.SaveUser(user)
	if err != nil {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}

	sms.SMS.SendSMS(user.Phone, "Новый пароль: " + password)
	go func(phone, password string) {
		telegrambot.Log("new password for " + phone + "\n" +
			"password: " + password);
	}(args.Phone, password)


	return "", nil
}