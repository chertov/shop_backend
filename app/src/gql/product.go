package gql

import (
	"db/ProductsDB"
	"github.com/neelance/graphql-go"
)

type productResolver struct {
	product *ProductsDB.Product
	category *ProductsDB.Category
}
func (r productResolver) ID() graphql.ID {
	return toGraphID(r.product.BaseProps.Id)
}
func (r productResolver) Parent() graphql.ID {
	if r.category == nil {
		return graphql.ID("")
	}
	return toGraphID(r.category.BaseFolder.BaseProps.Id)
}
func (r* productResolver) SetParent(category *ProductsDB.Category) {
	r.category = category
}
func (r productResolver) ID_uint64() uint64 {
	return r.product.BaseProps.Id
}
func (r productResolver) ID_string() string {
	return string(r.ID())
}
func (r productResolver) Title() string {
	return r.product.BaseProps.Title
}
func (r productResolver) ShortTitle() string {
	return r.product.BaseProps.ShortTitle
}
func (r productResolver) Summary() string {
	return r.product.BaseProps.Summary
}
func (r productResolver) Description() string {
	return r.product.BaseProps.Description
}
func (r productResolver) ShowOnSite() bool {
	return r.product.BaseProps.ShowOnSite
}

func (r productResolver) Images() []graphql.ID {
	ids := make([]graphql.ID, 0)
	for _, id := range r.product.Images {
		ids = append(ids, toGraphIDstr(id))
	}
	return ids
}

func (r *productResolver) Models() []graphql.ID {
	models := make([]graphql.ID, 0)
	models = append(models, toGraphID(r.product.BaseModel.BaseProps.Id))
	for _, pmodel := range r.product.Models {
		model := db.Models[pmodel.BaseProps.Id]
		if model.ShowOnSite() {
			models = append(models, model.ID())
		}
	}
	return models
}
