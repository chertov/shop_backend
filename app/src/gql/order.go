package gql

import (
	"context"
	"github.com/neelance/graphql-go"
	"log"
	"strconv"
	"telegrambot"
	"kvdb"
	"kinds"
	"tools"
	"errs"
)

func OrderFormSes(ses kvdb.Session) kinds.Order {
	order := kinds.NewOrder()
	err := order.FromTNT(ses.Order)
	if err != nil {
		log.Println("OrderFormSes",err)
	}
	items := make([]*kinds.OrderItem, 0)
	for _, item := range order.Items {
		id, err := strconv.ParseUint(item.ModelId, 10, 64)
		_, ok := db.Models[id]
		if err == nil && ok {
			items = append(items, item)
		}
	}
	order.Items = items
	return order
}

func (r Resolver) GetOrder(ctx context.Context) (*OrderResolver, error) {
	ses, ok, err := kvdb.GetFromContext(ctx)
	if err != nil {
		log.Println("GetOrder GetFromContext", err)
		return nil, err
	}
	if !ok {
		log.Println("GetOrder GetFromContext not ok")
		return nil, nil
	}
	order := OrderFormSes(ses)
	return &OrderResolver{order}, nil
}

func (r Resolver) MakeOrder(ctx context.Context, args *struct{ Order *kinds.Order }) (*OrderResolver, error) {
	if args == nil {
		return nil, nil
	}
	if args.Order == nil {
		return nil, nil
	}
	ses, ok, err := kvdb.GetFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, nil
	}
	var user *kinds.User
	user = nil
	if ses.User != nil {
		err = user.FromTNT(ses.User)
		if err != nil {
			log.Println("Login user.FromTNT error ", err)
			return nil, err
		}
	}

	order := args.Order
	validationError := order.Check()
	if validationError != nil {
		return nil, validationError
	}

	phoneUser, err := kvdb.GetUserByPhone(order.Phone)
	if err != nil {
		return nil, err
	}

	if phoneUser != nil && user == nil {
		return nil, errs.PHONE_EXISTS_USER_ISNOT_AUTH.ToFrontend()
	}
	if phoneUser != nil && user != nil && phoneUser.Id != user.Id {
		return nil, errs.PHONE_BELONGS_TO_ANOTHER_USER.ToFrontend()
	}

	for _, orderItem := range order.Items {
		if orderItem == nil {
			continue
		}
		uint64_id, err := fromGraphID(graphql.ID(orderItem.ModelId))
		if err != nil {
			continue
		}
		model, ok := db.Models[uint64_id]
		if !ok {
			continue
		}
		orderItem.ModelId = model.ID_uint64_str()
		orderItem.Article = model.Article()
		orderItem.Code = model.Code()
		orderItem.Title = model.Title()
		orderItem.PTitle = model.product.BaseProps.Title
		orderItem.Price.Currency = model.Price().Currency()
		orderItem.Price.Value = model.Price().Value()

		// order.Items = append(order.Items, orderItem)
	}
	order.Messages = make([]kinds.OrderMessage, 0)

	if user == nil {
		order.User = tools.RandStringRunes(5)
		order.NewUserPassword = tools.NewPassword()
		go func(order kinds.Order) {
			telegrambot.Log("new order " + strconv.FormatInt(int64(order.Number), 10) + "\n" +
				"phone: " + order.Phone + " " + "password: " + order.NewUserPassword);
		}(*order)
	} else {
		order.User = user.Id
	}
	err = kvdb.MakeOrder(order)
	if err != nil {
		return nil, err
	}

	ses.Order = nil
	kvdb.SaveSession(ses)

	return &OrderResolver{*order}, nil
}

func (r Resolver) UpdateOrder(ctx context.Context, args *struct{ Order *kinds.Order }) (*OrderResolver, error) {
	if args == nil {
		return nil, nil
	}
	if args.Order == nil {
		return nil, nil
	}
	ses, ok, err := kvdb.GetFromContext(ctx)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, nil
	}

	order := args.Order
	//validationError := order.Check()
	items := make([]*kinds.OrderItem, 0)
	for _, orderItem := range order.Items {
		if orderItem == nil {
			continue
		}
		uint64_id, err := fromGraphID(graphql.ID(orderItem.ModelId))
		if err != nil {
			continue
		}
		model, ok := db.Models[uint64_id]
		if !ok {
			continue
		}
		orderItem.ModelId = model.ID_uint64_str()
		orderItem.Article = model.Article()
		orderItem.Code = model.Code()
		orderItem.Title = model.Title()
		orderItem.PTitle = model.product.BaseProps.Title
		orderItem.Price.Currency = model.Price().Currency()
		orderItem.Price.Value = model.Price().Value()

		items = append(items, orderItem)
	}
	order.Items = items
	order.Messages = make([]kinds.OrderMessage, 0)
	ses.Order = order
	kvdb.SaveSession(ses)
	order.Items = items
	return &OrderResolver{*order}, nil
}
