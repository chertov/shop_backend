package gql

import (
	"db/ProductsDB"
	"github.com/neelance/graphql-go"
	"strconv"
)

type enumValueResolver struct {
	id uint64
	value string
}
func (r enumValueResolver) ID() graphql.ID {
	return toGraphID(r.id)
}
func (r enumValueResolver) Value() string {
	return r.value
}

type enumResolver struct {
	enum *ProductsDB.Enum
}
func (r enumResolver) Type() string {
	switch r.enum.Type {
	case ProductsDB.Enum_ENUM_INT:
		return "INT"
	case ProductsDB.Enum_ENUM_REAL:
		return "REAL"
	case ProductsDB.Enum_ENUM_STRING:
		return "STR"
	default:
		panic("invalid enum type")
	}
}
func (r enumResolver) Values() []*enumValueResolver {
	values := make([]*enumValueResolver, 0)
	for _, value := range r.enum.Values {
		switch r.enum.Type {
		case ProductsDB.Enum_ENUM_INT:
			values = append(values, &enumValueResolver{value.Id, strconv.FormatInt(int64(value.GetInt()),10)})
		case ProductsDB.Enum_ENUM_REAL:
			values = append(values, &enumValueResolver{value.Id, strconv.FormatFloat(value.GetReal(),'f',2,64)})
		case ProductsDB.Enum_ENUM_STRING:
			values = append(values, &enumValueResolver{value.Id, value.GetStr()})
		default:
			panic("invalid enum type")
		}
	}
	return values
}


type propertyResolver struct {
	property *ProductsDB.Property
}
func (r propertyResolver) ID() graphql.ID {
	return toGraphID(r.property.Id)
}
func (r propertyResolver) Type() string {
	switch r.property.Type {
	case ProductsDB.Property_BOOL:
		return "BOOL"
	case ProductsDB.Property_INT:
		return "INT"
	case ProductsDB.Property_REAL:
		return "REAL"
	case ProductsDB.Property_ENUM:
		return "ENUM"
	case ProductsDB.Property_MULTIENUM:
		return "MULTIENUM"
	default:
		panic("invalid unit")
	}
}
func (r propertyResolver) TypePBF() ProductsDB.Property_PropertyType {
	return r.property.Type
}
func (r propertyResolver) Title() string {
	return r.property.Title
}
func (r propertyResolver) Description() string {
	return r.property.Description
}
func (r propertyResolver) Enum() *enumResolver {
	if r.property.Enum == nil {
		return nil
	}
	return &enumResolver{r.property.Enum}
}


type propertyValueResolver struct {
	value *ProductsDB.PropertyValue
}
func (r propertyValueResolver) ID() graphql.ID {
	return toGraphID(r.value.PropertyId)
}
func (r propertyValueResolver) Value() string {
	property, ok := db.Properties[r.value.PropertyId]
	if !ok {
		return ""
	}
	switch property.TypePBF() {
	case ProductsDB.Property_BOOL:
		if r.value.Bool {
			return "true"
		} else {
			return "false"
		}
	case ProductsDB.Property_INT:
		return strconv.FormatInt(r.value.Int, 10)
	case ProductsDB.Property_REAL:
		return strconv.FormatFloat(r.value.Real,'f',3, 64)
	case ProductsDB.Property_ENUM:
		return string(toGraphID(r.value.Enum))
	case ProductsDB.Property_MULTIENUM: {
		return ""
	}
	default:
		panic("invalid unit")
	}
	return ""
}
func (r propertyValueResolver) Values() []graphql.ID {
	values := make([]graphql.ID, 0)
	property, ok := db.Properties[r.value.PropertyId]
	if !ok {
		return values
	}
	switch property.TypePBF() {
	case ProductsDB.Property_BOOL:
		break
	case ProductsDB.Property_INT:
		break
	case ProductsDB.Property_REAL:
		break
	case ProductsDB.Property_ENUM:
		break
	case ProductsDB.Property_MULTIENUM: {
		for _, value := range r.value.MultiEnum {
			values = append(values, toGraphID(value))
		}
	}
	default:
		panic("invalid unit")
	}
	return values
}
