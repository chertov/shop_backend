package gql

import (
	"github.com/neelance/graphql-go"
	"db/ProductsDB"
	"strconv"
)

type modelResolver struct {
	model *ProductsDB.Model
	product *ProductsDB.Product
	propertySet *ProductsDB.PropertySet
}
func (r *modelResolver) SetPropertySet(propertySet *ProductsDB.PropertySet) {
	r.propertySet = propertySet
}
func (r modelResolver) ID() graphql.ID {
	return toGraphID(r.model.BaseProps.GetId())
}
func (r modelResolver) ID_uint64() uint64 {
	return r.model.BaseProps.GetId()
}
func (r modelResolver) ID_uint64_str() string {
	return strconv.FormatUint(r.model.BaseProps.GetId(), 10)
}
func (r modelResolver) ID_string() string {
	return string(r.ID())
}
func (r modelResolver) Product() graphql.ID {
	return toGraphID(r.product.GetBaseProps().Id)
}
func (r modelResolver) Parent() graphql.ID {
	if r.product == nil {
		return graphql.ID("")
	}
	return toGraphID(r.product.GetBaseProps().Id)
}
func (r modelResolver) Title() string {
	return r.model.BaseProps.Title
}
func (r modelResolver) ShortTitle() string {
	return r.model.BaseProps.ShortTitle
}
func (r modelResolver) Summary() string {
	return r.model.BaseProps.Summary
}
func (r modelResolver) Description() string {
	return r.model.BaseProps.Description
}
func (r modelResolver) ShowOnSite() bool {
	return r.model.BaseProps.ShowOnSite
}
func (r modelResolver) Code() string {
	return r.model.Code
}
func (r modelResolver) Article() string {
	return string(r.model.Article)
}
func (r modelResolver) Images() []graphql.ID {
	ids := make([]graphql.ID, 0)
	for _, id := range r.model.Images {
		ids = append(ids, toGraphIDstr(id))
	}
	return ids
}
func (r modelResolver) Properties() []*propertyValueResolver {
	properties := make([]*propertyValueResolver, 0)
	for _, value := range r.model.Properties {
		properties = append(properties, &propertyValueResolver{value})
	}
	return properties
}

func (r modelResolver) Volume() int32 {
	return int32(0)
}
func (r modelResolver) Price() *priceResolver {
	price, ok := db.Prices[r.Article()]
	if !ok {
		price = 0.0
	}
	return &priceResolver{price, "RUB"}
}
