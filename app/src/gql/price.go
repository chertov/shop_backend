package gql


type priceResolver struct {
	value 		float64
	currency	string
}
func (r *priceResolver) Currency() string {
	//switch r.price.Type {
	//case ItemsDB.PriceCurrency_PRICE_RUB:
	//	return "RUB"
	//case ItemsDB.PriceCurrency_PRICE_USD:
	//	return "USD"
	//case ItemsDB.PriceCurrency_PRICE_EUR:
	//	return "EUR"
	//default:
	//	panic("invalid unit")
	//}
	return r.currency
}
func (r *priceResolver) Value() float64 {
	//if r.price.Value < 0 {
	//	return 0.0
	//}
	//return float64(r.price.Value)
	return r.value
}
