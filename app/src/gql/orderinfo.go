package gql

import (
	"github.com/neelance/graphql-go"
	"strconv"
	"errors"
	"encoding/hex"
	"crypto/md5"
	"tools"
	"kinds"
)

type OrderItemResolver struct {
	i *kinds.OrderItem
}
func (r OrderItemResolver) ModelId() (graphql.ID, error) {
	id, err := fromGraphID(graphql.ID(r.i.ModelId))
	if err != nil {
		return "", err
	}
	_, ok := db.Models[id]
	if ok {
		return toGraphID(id), nil
	}
	return "", errors.New("Can't find item with ID " + strconv.FormatUint(id,10))
}
func (r OrderItemResolver) Count() int32 {
	return r.i.Count
}
func (r OrderItemResolver) Article() string {
	return r.i.Article
}
func (r OrderItemResolver) Code() string {
	return r.i.Code
}
func (r OrderItemResolver) PTitle() string {
	return r.i.PTitle
}
func (r OrderItemResolver) Title() string {
	return r.i.Title
}
func (r OrderItemResolver) Price() *priceResolver {
	return &priceResolver{float64(r.i.Price.Value), "RUB"}
}

type CompanyinfoResolver struct {
	h *kinds.Companyinfo
}
func (r CompanyinfoResolver) Name() string {
	return r.h.Name
}
func (r CompanyinfoResolver) Phone() string {
	return r.h.Phone
}
func (r CompanyinfoResolver) JurAddress() string {
	return r.h.JurAddress
}
func (r CompanyinfoResolver) FactAddress() string {
	return r.h.FactAddress
}
func (r CompanyinfoResolver) INN() string {
	return r.h.INN
}
func (r CompanyinfoResolver) KPP() string {
	return r.h.KPP
}
func (r CompanyinfoResolver) OKPO() string {
	return r.h.OKPO
}
func (r CompanyinfoResolver) KorrSchet() string {
	return r.h.KorrSchet
}
func (r CompanyinfoResolver) RaschSchet() string {
	return r.h.RaschSchet
}
func (r CompanyinfoResolver) BIK() string {
	return r.h.BIK
}
func (r CompanyinfoResolver) Bank() string {
	return r.h.Bank
}


type FizikResolver struct {
	h *kinds.Fizik
}
func (r FizikResolver) Passport() string {
	return r.h.Passport
}

type DeliveryResolver struct {
	h *kinds.Delivery
}
func (r DeliveryResolver) Type() string {
	return r.h.Type
}
func (r DeliveryResolver) Address() string {
	return r.h.Address
}

type OrderMessageResolver struct {
	h kinds.OrderMessage
}
func (r OrderMessageResolver) Time() int32 {
	return int32(r.h.Time)
}
func (r OrderMessageResolver) Message() string {
	return r.h.Message
}


type OrderResolver struct {
	i kinds.Order
}
func (r OrderResolver) Id() graphql.ID {
	return graphql.ID(r.i.Id)
}
func (r OrderResolver) Number() int32 {
	return r.i.Number
}
func (r OrderResolver) TimeAdd() int32 {
	return int32(r.i.TimeAdd)
}
func (r OrderResolver) TimeChange() int32 {
	return int32(r.i.TimeChange)
}

func (r OrderResolver) Name() string {
	return r.i.Name
}
func (r OrderResolver) Surname() string {
	return r.i.Surname
}
func (r OrderResolver) Patronymic() string {
	return r.i.Patronymic
}
func (r OrderResolver) Phone() string {
	return r.i.Phone
}
func (r OrderResolver) Email() string {
	return r.i.Email
}
func (r OrderResolver) Comment() string {
	return r.i.Comment
}

func (r OrderResolver) IsCompany() bool {
	return r.i.IsCompany
}
func (r OrderResolver) Fizik() *FizikResolver {
	return &FizikResolver{r.i.Fizik}
}
func (r OrderResolver) Company() *CompanyinfoResolver {
	return &CompanyinfoResolver{r.i.Company}
}

func (r OrderResolver) Delivery() *DeliveryResolver {
	return &DeliveryResolver{r.i.Delivery}
}

func (r OrderResolver) State() string {
	return r.i.State
}
func (r OrderResolver) Messages() []*OrderMessageResolver {
	messages := make([]*OrderMessageResolver, 0)
	for _, message := range r.i.Messages {
		messages = append(messages, &OrderMessageResolver{message})
	}
	return messages
}
func (r OrderResolver) Items() []*OrderItemResolver {
	items := make([]*OrderItemResolver, 0)
	for _, item := range r.i.Items {
		items = append(items, &OrderItemResolver{item})
	}
	return items
}

func (r OrderResolver) ConfimCode() string {
	rndstr := tools.RandStringRunes(10)
	md5Sum := md5.Sum([]byte(r.i.NewUserPassword + rndstr))
	return hex.EncodeToString(md5Sum[:]) + ":" + rndstr
}