package gql

import (
	"db/ProductsDB"
	"encoding/binary"
	"encoding/base64"
	"github.com/golang/protobuf/proto"
	"github.com/neelance/graphql-go"
	"gopkg.in/yaml.v2"
	"context"
	"io/ioutil"
	"log"
	"runtime"
	. "panic"
	"time"
	"app/auth"
	"errors"
	"strconv"
	"fmt"
)

type ProductsData struct {
	Root        *ProductsDB.FoldersCategories
	Folders     map[uint64]*folderResolver
	Categories  map[uint64]*categoryResolver
	Products    map[uint64]*productResolver
	Models      map[uint64]*modelResolver
	Properties  map[uint64]*propertyResolver
	AltIds      map[uint64]string
	IdByAlt     map[string]uint64

	Prices		map[string]float64
	Volume		map[string]uint64
}
func NewDB(root *ProductsDB.FoldersCategories) *ProductsData {
	db := ProductsData{}

	db.Root = root
	db.Folders = make(map[uint64]*folderResolver)
	db.Categories = make(map[uint64]*categoryResolver)
	db.Products = make(map[uint64]*productResolver)
	db.Models = make(map[uint64]*modelResolver)
	db.Properties = make(map[uint64]*propertyResolver)
	db.AltIds = make(map[uint64]string)
	db.IdByAlt = make(map[string]uint64)

	db.Prices = make(map[string]float64)
	db.Volume = make(map[string]uint64)
	return &db
}

var db *ProductsData

func toGraphID(id uint64) graphql.ID  {
	altid,ok := db.AltIds[id]
	if ok {
		return graphql.ID(altid)
	}
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, id)
	encoded := base64.URLEncoding.EncodeToString(b)
	return graphql.ID(encoded)
	// return graphql.ID(strconv.FormatUint(r.h.BaseFolder.BaseProps.Id, 10))
}
func toGraphIDstr(id uint64) graphql.ID  {
	return graphql.ID(strconv.FormatUint(id, 10))
}
func fromGraphID(id graphql.ID) (uint64, error) {
	uint64id, ok := db.IdByAlt[string(id)]
	if ok {
		return uint64id, nil
	}
	decoded, err := base64.URLEncoding.DecodeString(string(id))
	if err != nil {
		id, err_strint := strconv.ParseUint(string(id),10,64)
		if err_strint == nil {
			return id, nil
		}
		fmt.Println("decode error:", err)
		return 0, errors.New("incorrect id")
	}
	return binary.LittleEndian.Uint64(decoded), nil
}
func fromGraphIDstr(id graphql.ID) (string, bool) {
	uint64id, err := fromGraphID(id)
	if err != nil {
		uint64id, err = strconv.ParseUint(string(id), 10, 64)
		if err != nil {
			return "", false
		}
	}
	return strconv.FormatUint(uint64id, 10), true
}

func WatchPBF(pbf_path, prices_path string) {

}
func LoadPBF(pbf_path, prices_path string) error {

	t1 := time.Now()
	pbf := ProductsDB.Db{}
	{
		data, err := ioutil.ReadFile(pbf_path)
		if err != nil {
			return Panic(err)
		}
		err = proto.Unmarshal(data, &pbf)
		if err != nil {
			log.Fatalln("can't load: ", pbf_path, "\n", err)
			return Panic(err)
		}
	}
	runtime.GC()

	newdb := NewDB(pbf.Root)
	for altid, id := range pbf.AltIds {
		newdb.AltIds[id] = altid
		newdb.IdByAlt[altid] = id
	}

	{
		yamlFile, err := ioutil.ReadFile(prices_path)
		if err != nil {
			return Panic(err)
		}
		m := make(map[interface{}]interface{})
		err = yaml.Unmarshal(yamlFile, &m)
		if err != nil {
			log.Fatalf("Unmarshal: %v", err)
		}
		for k, v := range m {
			key, ok := k.(string)
			if !ok {
				log.Fatalf("Error k.(string)", err)
			}
			if key == "prices" {
				article_map, _ := v.(map[interface{}]interface{})
				for k, v := range article_map {
					article, ok := k.(string)
					if !ok {
						log.Fatalf("Can't cast article to string")
					}
					price, ok := v.(int)
					if !ok {
						log.Fatalf("Can't cast price to int")
					}
					newdb.Prices[article] = float64(price)
				}
			}
		}
	}

	for _, property_pbf := range pbf.Properties {
		newdb.Properties[property_pbf.Id] = &propertyResolver{property_pbf}
	}

	for _, folder_pbf := range pbf.Folders {
		newdb.Folders[folder_pbf.BaseFolder.BaseProps.Id] = &folderResolver{folder_pbf, nil}
	}
	for _, category_pbf := range pbf.Categories {
		newdb.Categories[category_pbf.BaseFolder.BaseProps.Id] = &categoryResolver{category_pbf, nil}
	}
	for _, product_pbf := range pbf.Products {
		newdb.Products[product_pbf.BaseProps.Id] = &productResolver{product_pbf, nil}
		newdb.Models[product_pbf.BaseModel.BaseProps.Id] = &modelResolver{product_pbf.BaseModel,product_pbf, nil}
		for _, model_pbf := range product_pbf.Models {
			newdb.Models[model_pbf.BaseProps.Id] = &modelResolver{model_pbf,product_pbf, nil}
		}
	}

	t2 := time.Now()
	log.Println("time load pbf ", t2.Sub(t1))
	runtime.GC()
	db = newdb

	for _, folder := range db.Folders {
		folder.SetToChilds()
	}
	for _, category := range db.Categories {
		category.SetToChilds()
	}

	return nil
}

type Resolver struct{}
func (r Resolver) Root(ctx context.Context) (*rootResolver, error) {
	return &rootResolver{}, nil
}
func (r Resolver) RootFolders(ctx context.Context) ([]graphql.ID, error) {
	folders := make([]graphql.ID, 0)
	for _, id := range db.Root.Folders {
		folder := db.Folders[id]
		if folder.ShowOnSite() {
			folders = append(folders, folder.ID())
		}
	}
	return folders, nil
}
func (r Resolver) RootCategories(ctx context.Context) ([]graphql.ID, error) {
	categories := make([]graphql.ID, 0)
	for _, id := range db.Root.Categories {
		category := db.Categories[id]
		if category.ShowOnSite() {
			categories = append(categories, category.ID())
		}
	}
	return categories, nil
}

func (r Resolver) Categories(ctx context.Context) ([]*categoryResolver, error) {
	categories := make([]*categoryResolver, 0)
	for _, category := range db.Categories {
		if category.ShowOnSite() {
			categories = append(categories, category)
		}
	}
	return categories, nil
}
func (r Resolver) Folders(ctx context.Context) ([]*folderResolver, error) {
	folders := make([]*folderResolver, 0)
	for _, folder := range db.Folders {
		if folder.ShowOnSite() {
			folders = append(folders, folder)
		}
	}
	return folders, nil
}
func (r Resolver) Products(ctx context.Context) ([]*productResolver, error) {
	products := make([]*productResolver, 0)
	for _, product := range db.Products {
		products = append(products, product)
	}
	return products, nil
}
func (r Resolver) Models(ctx context.Context) ([]*modelResolver, error) {
	models := make([]*modelResolver, 0)
	for _, model := range db.Models {
		models = append(models, model)
	}
	return models, nil
}


//func (r Resolver) Folder(ctx context.Context, args *struct{ID graphql.ID}) (*folderResolver, error) {
//	id, _ := fromGraphID(args.ID)
//	folder := db.Folders[id]
//	return folder, nil
//}
//func (r Resolver) Category(ctx context.Context, args *struct{ID graphql.ID}) (*categoryResolver, error) {
//	ses := ctx.Value("ses")
//	log.Println("Category ses:",ses)
//	id, _ := fromGraphID(args.ID)
//	category := db.Categories[id]
//	//if(category.ShowOnSite()) {
//	//	categories = append(categories, category)
//	//}
//	return category, nil
//}
//func (r Resolver) Product(ctx context.Context, args *struct{ID graphql.ID}) (*productResolver, error) {
//	id, _ := fromGraphID(args.ID)
//	product := db.Products[id]
//	return product, nil
//}
//func (r Resolver) Model(ctx context.Context, args *struct{ID graphql.ID}) (*modelResolver, error) {
//	id, _ := fromGraphID(args.ID)
//	model := db.Models[id]
//	return model, nil
//}
func (r Resolver) Auth() *auth.AuthResolver {
	return &auth.AuthResolver{}
}