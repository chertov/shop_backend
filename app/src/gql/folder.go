package gql

import (
	"github.com/neelance/graphql-go"
	"db/ProductsDB"
)

type folderResolver struct {
	h *ProductsDB.Folder
	folder *ProductsDB.Folder
}
func (r folderResolver) ID() graphql.ID {
	return toGraphID(r.h.BaseFolder.BaseProps.Id)
}
func (r folderResolver) IDuint64() uint64 {
	return r.h.BaseFolder.BaseProps.Id
}
func (r folderResolver) Parent() graphql.ID {
	if r.folder == nil {
		return graphql.ID("")
	}
	return toGraphID(r.folder.BaseFolder.BaseProps.Id)
}
func (r* folderResolver) SetParent(folder *ProductsDB.Folder) {
	r.folder = folder
}
func (r folderResolver) Title() string {
	return r.h.BaseFolder.BaseProps.Title
}
func (r folderResolver) ShortTitle() string {
	return r.h.BaseFolder.BaseProps.ShortTitle
}
func (r folderResolver) Summary() string {
	return r.h.BaseFolder.BaseProps.Summary
}
func (r folderResolver) Description() string {
	return r.h.BaseFolder.BaseProps.Description
}
func (r folderResolver) ShowOnSite() bool {
	return r.h.BaseFolder.BaseProps.ShowOnSite
}
func (r folderResolver) Image() graphql.ID {
	return toGraphIDstr(r.h.BaseFolder.Image)
}
func (r folderResolver) Folders() []graphql.ID {
	folders := make([]graphql.ID, 0)
	for _, id := range r.h.FoldersCategories.Folders {
		folder := db.Folders[id]
		if folder.ShowOnSite() {
			folders = append(folders, folder.ID())
		}
	}
	return folders
}
func (r folderResolver) Categories() []graphql.ID {
	categories := make([]graphql.ID, 0)
	for _, id := range r.h.FoldersCategories.Categories {
		category := db.Categories[id]
		if category.ShowOnSite() {
			categories = append(categories, category.ID())
		}
	}
	return categories
}

func (r folderResolver) SetToChilds() {
	for _, id := range r.h.FoldersCategories.Folders {
		folder := db.Folders[id]
		folder.SetParent(r.h)
	}
	for _, id := range r.h.FoldersCategories.Categories {
		category := db.Categories[id]
		category.SetParent(r.h)
	}
}