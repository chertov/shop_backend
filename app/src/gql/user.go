package gql

import (
	"context"
	"log"
	"kinds"
	"kvdb"
	"errs"
)

type userResolver struct {
	h *kinds.User
}

func (r userResolver) Ordershistory(ctx context.Context) ([]*OrderResolver, error) {
	if r.h == nil {
		return nil, errs.INTERNAL_ERROR.ToFrontend()
	}
	ordersResolvers:=make([]*OrderResolver,0)
	orders, err := kvdb.GetOrders(r.h.Id)
	if err != nil {
		log.Println(err)
	}
	for _, resOrder := range orders {
		order := kinds.NewOrder()
		order.FromTNT(resOrder)
		ordersResolvers = append(ordersResolvers, &OrderResolver{order})
	}
	return ordersResolvers, nil
}
func (r userResolver) Name(ctx context.Context) (string, error) {
	if r.h == nil {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}
	return r.h.Name, nil
}
func (r userResolver) FamilyName(ctx context.Context) (string, error) {
	if r.h == nil {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}
	return r.h.Surname, nil
}
func (r userResolver) Email(ctx context.Context) (string, error) {
	if r.h == nil {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}
	return r.h.Email, nil
}
func (r userResolver) Phone(ctx context.Context) (string, error) {
	if r.h == nil {
		return "", errs.INTERNAL_ERROR.ToFrontend()
	}
	return r.h.Phone, nil
}
