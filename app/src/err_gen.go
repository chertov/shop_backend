package main

import (
	"log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"crypto/md5"
	// "encoding/json"
	"strconv"
	"strings"
	"os"
	"encoding/hex"
)
type Error struct {
	Code int32
	Name string
	Description string
	Frontend    bool
	Logging     bool
}
type ErrorsFile struct {
	StartCode int32
	Errors map[string]Error
	OrderedErrors []Error
}

//go:generate go run err_gen.go app/
func main() {

	errorsPath := "../../../tarantool/tarantool_lua/errors.yml"
	apppath := ""
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) > 0 {
		apppath = argsWithoutProg[0]
	}
	errorsPath = apppath + errorsPath

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	data, err := ioutil.ReadFile(errorsPath)
	if err != nil {
		log.Fatalln(err)
	}
	md5Sum := md5.Sum(data)
	errorsFile := ErrorsFile{}
	errorsFile.OrderedErrors = make([]Error, 0)
	yaml.Unmarshal(data, &errorsFile)
	if err != nil {
		log.Fatalln(err)
	}
	slice := yaml.MapSlice{}
	yaml.Unmarshal(data, &slice)
	if err != nil {
		log.Fatalln(err)
	}

	ymlErrors := yaml.MapSlice{}
	for index, v := range slice {
		if v.Key.(string) == "errors" {
			ymlErrors = slice[index].Value.(yaml.MapSlice)
			break
		}
	}

	for index, element := range ymlErrors {
		key := element.Key.(string)
		error, ok := errorsFile.Errors[key]
		if ok {
			error.Code = int32(index) + errorsFile.StartCode
			error.Name = key
			errorsFile.Errors[key] = error
			errorsFile.OrderedErrors = append(errorsFile.OrderedErrors, error)
		}
	}

	// bytes, err := json.MarshalIndent(&errorsFile, "", "    ")
	// log.Println(string(bytes))

	str := "package errs\n"
	str += "\n"
	str += "type Error struct{\n"
	str += "	Code int32\n"
	str += "	Name string\n"
	str += "	Description string\n"
	str += "	Frontend bool\n"
	str += "	Logging bool\n"
	str += "}\n"
	str += "\n"
	str += "type ErrorsData struct {\n"
	str += "	StartCode int32\n"
	str += "	Errors map[string]Error\n"
	str += "}\n"
	str += "\n"
	str += "const md5Sum string = \"" + hex.EncodeToString(md5Sum[:]) + "\"\n"
	str += "const StartCode int32 = " + strconv.FormatInt(int64(errorsFile.StartCode), 10) + "\n"
	str += "\n"
	for _, errorValue := range errorsFile.OrderedErrors {
		str += "var " + errorValue.Name + " Error = Error{"
		str += strconv.FormatInt(int64(errorValue.Code), 10) + ", "
		str += "\"" + errorValue.Name + "\", "
		str += "\"" + errorValue.Description + "\", "
		str += strconv.FormatBool(errorValue.Logging) + ", "
		str += strconv.FormatBool(errorValue.Frontend) + ""
		str += "}\n"
	}
	str += "\n"
	str += "var Data ErrorsData = ErrorsData{\n"
	str += "    " + strconv.FormatInt(int64(errorsFile.StartCode), 10) + ",\n"
	str += "    map[string]Error{\n"
	errs := make([]string, 0)
	for _, error := range errorsFile.OrderedErrors {
		errs = append(errs, "        \"" + error.Name + "\": " + error.Name)
	}
	str += strings.Join(errs[:],",\n") + " "
	str += " }}\n"
	str += "\n"
	str += "var Errors map[string]Error = map[string]Error{\n"
	str += strings.Join(errs[:],",\n") + " "
	str += "}\n"

	os.MkdirAll(apppath + "../errs/", os.ModePerm)
	err = ioutil.WriteFile(apppath + "../errs/errors.gen.go", []byte(str), 0644)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(apppath + "../errs/errors.gen.go was generated")
}
