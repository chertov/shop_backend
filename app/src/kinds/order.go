package kinds

import (
	"encoding/json"
	"github.com/mitchellh/mapstructure"
	"errors"
	"regexp"
	"errs"
	"strings"
)

type OrderItem struct {
	ModelId string
	Count 	int32
	Article	string
	Code 	string
	PTitle 	string
	Title 	string
	Price 	struct{Currency string; Value float64}
}
type Companyinfo struct{
	Name        string
	Phone       string
	JurAddress  string
	FactAddress string
	INN         string
	KPP         string
	OKPO        string
	KorrSchet   string
	RaschSchet  string
	BIK         string
	Bank        string
}
var innCheck = regexp.MustCompile(`^([0-9]{10}|[0-9]{12})$`)
var kppCheck = regexp.MustCompile(`^[0-9]{9}$`)
var okpoCheck = regexp.MustCompile(`^([0-9]{8}|[0-9]{10})$`)
var korrCheck = regexp.MustCompile(`^301[0-9]{17}$`)
var raschCheck = regexp.MustCompile(`^[0-9]{20}$`)
var bikCheck = regexp.MustCompile(`^04[0-9]{7}$`)
func (r Companyinfo) Check() []string {
	errors := make([]string, 0)
	if !innCheck.MatchString(r.INN) {
		errors = append(errors, errs.INN_ISNT_VALID.ToFrontend().Error())
	}
	if !kppCheck.MatchString(r.KPP) {
		errors = append(errors, errs.KPP_ISNT_VALID.ToFrontend().Error())
	}
	if !okpoCheck.MatchString(r.OKPO) {
		errors = append(errors, errs.OKPO_ISNT_VALID.ToFrontend().Error())
	}
	if !korrCheck.MatchString(r.KorrSchet) {
		errors = append(errors, errs.KORR_ISNT_VALID.ToFrontend().Error())
	}
	if !raschCheck.MatchString(r.RaschSchet) {
		errors = append(errors, errs.RASCH_ISNT_VALID.ToFrontend().Error())
	}
	if !bikCheck.MatchString(r.BIK) {
		errors = append(errors, errs.BIK_ISNT_VALID.ToFrontend().Error())
	}

	return errors
}


type Fizik struct {
	Passport string
}
var passportCheck = regexp.MustCompile(`^[0-9]{4}[0-9]{6}$`) // 4 цифры серия, 6 цифр номер
func (r Fizik) Check() []string {
	if !passportCheck.MatchString(r.Passport) {
		return []string{errs.PASSPORT_ISNT_VALID.ToFrontend().Error()}
	}
	return []string{}
}
type Delivery struct{
	Type    string
	Address string
}
func (r Delivery) Check() []string {
	if r.Type != "" && r.Type != "pochta-rossii" && r.Type != "pick-point" {
		return []string{errs.DELIVERYTYPE_ISNT_VALID.ToFrontend().Error()}
	}
	return []string{}
}
type OrderMessage struct{
	Time 	int32
	Message string
}

type Order struct {
	Id		    string
	Number 	    int32
	TimeAdd	    int32
	TimeChange	int32

	Name        string
	Surname     string
	Patronymic  string

	Phone       string
	Email       string
	Comment     string

	IsCompany   bool
	Company     *Companyinfo
	Fizik       *Fizik

	Delivery    *Delivery

	State       string
	Messages    []OrderMessage
	Items       []*OrderItem

	User		string
	NewUserPassword string		`datastore:"-"`
	Day			string
}

func NewOrder() Order {
	r := Order{}
	r.Company = &Companyinfo{}
	r.Fizik = &Fizik{}
	r.Delivery = &Delivery{}
	r.Items = make([]*OrderItem,0)
	r.Messages = make([]OrderMessage,0)
	return r
}

var phoneCheck = regexp.MustCompile(`^\+7[0-9]{10}$`)
func (r Order) Check() error {
	orderErrors := make([]string, 0)
	if !phoneCheck.MatchString(r.Phone) {
		orderErrors = append(orderErrors, errs.PHONE_ISNT_VALID.ToFrontend().Error())
	}
	deliveryErrors := r.Delivery.Check()
	orderErrors = append(orderErrors, deliveryErrors...)
	if r.IsCompany {
		err := r.Company.Check()
		orderErrors = append(orderErrors, err...)
	} else {
		// err := r.Fizik.Check()
		// orderErrors = append(orderErrors, err...)
	}

	if len(orderErrors) == 0 {
		return nil
	}
	return errors.New("[" + strings.Join(orderErrors,",") + "]")
}
func (r Order) ToJSON() string {
	b, _ := json.MarshalIndent(r, "", "    ")
	return string(b)
}
func (r *Order) FromTNT(data interface{}) error {
	err := mapstructure.Decode(data, r)
	if err != nil {
		return err
	}
	return nil
}
