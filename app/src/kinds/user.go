package kinds

import (
	"github.com/mitchellh/mapstructure"
	"log"
)

type User struct {
	Id			string

	Email		string
	Phone		string

	Name        string
	Surname     string
	Patronymic  string

	IsCompany   bool
	Company     *Companyinfo
	Fizik       *Fizik

	Delivery    *Delivery

	PasswordHash	string
}

func (r *User) FromTNT(data interface{}) error {
	err := mapstructure.Decode(data, r)
	if err != nil {
		log.Println("userFromTNT", err)
		return err
	}
	return nil
}
func UserFromTNT(data interface{}) User {
	user := User{}
	err := mapstructure.Decode(data, &user)
	if err != nil {
		log.Println("userFromTNT", err)
	}
	return user
}
