package telegrambot

import (
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"strconv"
	"strings"
)

var bot *tgbotapi.BotAPI

var sendId int64

func InitBot(toket string, getUpdates bool) error {
	var err error
	bot, err = tgbotapi.NewBotAPI(toket)
	if err != nil {
		return err
	}

	//bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	if (getUpdates) {
		// инициализируем канал, куда будут прилетать обновления от API
		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60
		updates, err := bot.GetUpdatesChan(u)
		if err != nil {
			return err
		}

		go func() {
			// читаем обновления из канала
			for update := range updates {
				log.Println(update.Message.Text)
				message_low := strings.ToLower(update.Message.Text)
				botName_low := strings.ToLower("@" + bot.Self.UserName + " test")

				if message_low == botName_low || message_low == "test" {
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, "from chat_id: "+strconv.FormatInt(int64(update.Message.Chat.ID), 10))
					msg.ReplyToMessageID = update.Message.MessageID
					bot.Send(msg)
				}
			}
		}()
	}
	return nil
}

func SendTo(chat_id int64, text string) {
	msg := tgbotapi.NewMessage(chat_id, text)
	bot.Send(msg)
}

func Log(text string) {
	msg := tgbotapi.NewMessage(sendId, text)
	bot.Send(msg)
}

func SetChatId(chat_id int64) {
	sendId = chat_id
}
