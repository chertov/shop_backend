package kvdb

import (
	"log"

	// Imports the Google Cloud Datastore client package.
	"cloud.google.com/go/datastore"
	"golang.org/x/oauth2/google"
	"golang.org/x/net/context"

	"kinds"
	"time"
	"crypto/md5"
	"encoding/hex"
)

type Task struct {
	Description string
}

func googleTx(client *datastore.Client, ctx context.Context) {

	tx, err := client.NewTransaction(ctx)
	if err != nil {
		log.Fatalf("client.NewTransaction: %v", err)
	}
	log.Println("Start tx...")

	order := kinds.NewOrder()
	order.Id = "order1"
	order.Name = "New tx name 1"
	if _, err := tx.Put(datastore.NameKey("Order", order.Id, nil), &order); err != nil {
		tx.Rollback()
		log.Fatalf("Failed to save task: %v", err)
	}

	time.Sleep(20 * time.Second)

	order.Id = "order2"
	order.Name = "New tx name 2"
	if _, err := tx.Put(datastore.NameKey("Order", order.Id, nil), &order); err != nil {
		tx.Rollback()
		log.Fatalf("Failed to save task: %v", err)
	}

	if _, err = tx.Commit(); err != nil {
		log.Fatalf("tx.Commit: %v", err)
	}
	log.Println("Commit tx!")
}
var ctx context.Context
var client *datastore.Client

func Init() error {
	// Use oauth2.NoContext if there isn't a good context to pass in.
	ctx = context.TODO()

	defCreds, err := google.FindDefaultCredentials(ctx, datastore.ScopeDatastore)
	if err != nil {
		log.Fatalf("Unable to find FindDefaultCredentials: %v", err)
		return err
	}

	// Creates a client.
	client, err = datastore.NewClient(ctx, defCreds.ProjectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
		return err
	}

	return nil
}


func GoogleMain() {

	order := kinds.NewOrder()
	order.Id = "order1"

	if err := SaveOrder(&order); err != nil {
		log.Fatalf("Failed to save task: %v", err)
	}
	orders := make([]kinds.Order, 0)
	query := datastore.NewQuery("Order")
	client.GetAll(ctx, query, &orders)
	log.Printf("Orders %v", orders)

	query = datastore.NewQuery("Order")
	keys, _ := client.GetAll(ctx, query, &orders)
	log.Printf("keys %v", keys)
	err := client.DeleteMulti(ctx, keys)
	if err != nil {
		log.Fatalf("Failed to DeleteMulti task: %v", err)
	}

	go googleTx(client, ctx)

	log.Println("Start tick!")
	c := time.Tick(1 * time.Second)
	for range c {
		orders := make([]kinds.Order, 0)
		query := datastore.NewQuery("Order")
		client.GetAll(ctx, query, &orders)
		log.Printf("tick %v", orders)
	}
	log.Println("Stop tick!")
}


func SaveOrder(order *kinds.Order) error {
	_, err := client.Put(ctx, datastore.NameKey("Order", order.Id, nil), order)
	if err != nil {
		log.Println("Error SaveOrder", err)
	}
	return err
}
func SaveUser(user *kinds.User) error {
	_, err := client.Put(ctx, datastore.NameKey("User", user.Id, nil), user)
	if err != nil {
		log.Println("Error SaveUser", err)
	}
	return err
}
func SaveUserOrder(user *kinds.User, order *kinds.Order) error {
	_, err := client.Put(ctx, datastore.NameKey("UserOrder", user.Id, nil), order)
	if err != nil {
		log.Println("Error SaveUserOrder", err)
	}
	return err
}

func MakeOrder(order *kinds.Order) error {
	tx, err := client.NewTransaction(ctx)
	if err != nil {
		log.Fatalf("client.NewTransaction: %v", err)
		return err
	}

	user := kinds.User{}
	user.Id = order.User
	user.Phone = order.Phone
	user.Name = order.Name
	user.Surname = order.Surname
	user.Patronymic = order.Patronymic
	user.IsCompany = order.IsCompany
	user.Fizik = order.Fizik
	user.Company = order.Company
	user.Delivery = order.Delivery

	md5sum := md5.Sum([]byte(order.Phone + order.NewUserPassword))
	var bytes []byte = md5sum[:]
	user.PasswordHash = hex.EncodeToString(bytes)
	SaveUser(&user)

	if _, err := tx.Put(datastore.NameKey("Order", order.Id, nil), order); err != nil {
		tx.Rollback()
		log.Fatalf("Failed to save order: %v", err)
		return err
	}
	if _, err := tx.Put(datastore.NameKey("User", user.Id, nil), &user); err != nil {
		tx.Rollback()
		log.Fatalf("Failed to save order: %v", err)
		return err
	}
	if _, err := tx.Commit(); err != nil {
		log.Fatalf("tx.Commit: %v", err)
		return err
	}
	return nil
}

func Login(phone, passwordhash string) (*kinds.User, *kinds.Order, error) {
	query := datastore.NewQuery("User").
		Filter("PasswordHash =", passwordhash).
		Filter("Phone =", phone).Limit(1)

	users := make([]kinds.User, 0)
	_, err := client.GetAll(ctx, query, &users)
	if err != nil {
		log.Fatalf("Login Failed to GetAll user: %v", err)
		return nil, nil, err
	}
	if len(users) != 1 {
		return nil, nil, nil
	}
	user := users[0]
	order := kinds.Order{}
	err = client.Get(ctx, datastore.NameKey("UserOrder", user.Id, nil), &order)
	if err != nil {
		if err == datastore.ErrNoSuchEntity {
			return &user, nil, nil
		}
		log.Fatalf("Login Failed to Get UserOrder: %v", err)
		return nil, nil, err
	}
	return &user, &order, nil
}
func GetOrders(userId string) ([]kinds.Order, error) {
	query := datastore.NewQuery("Order").
		Filter("User =", userId)

	orders := make([]kinds.Order, 0)
	_, err := client.GetAll(ctx, query, &orders)
	if err != nil {
		log.Fatalf("Login Failed to GetAll user: %v", err)
		return nil, err
	}
	return orders, nil
}
func GetUserByPhone(phone string) (*kinds.User, error) {
	query := datastore.NewQuery("User").
		Filter("Phone =", phone).Limit(1)

	users := make([]kinds.User, 0)
	_, err := client.GetAll(ctx, query, &users)
	if err != nil {
		log.Fatalf("GetUserByPhone Failed to GetAll user: %v", err)
		return nil, err
	}
	if len(users) != 1 {
		return nil, nil
	}
	return &users[0], nil
}

func MonitorConfig() {
	ReloadConfig()
	go func() {
		for {
			time.Sleep(time.Second * time.Duration(kinds.Config.ConfigReloadTime))
			ReloadConfig()
		}
	}()
}

func ReloadConfig() error {
	err := client.Get(ctx, datastore.NameKey("Config", "site", nil), &kinds.Config)
	if err != nil {
		log.Println("GetConfig Failed to Get Config:site", err)
		return err
	}

	err = client.Get(ctx, datastore.NameKey("Config", "templates", nil), &kinds.Config.Templates)
	if err != nil {
		log.Println("GetConfig Failed to Get Config:templates", err)
		return err
	}
	return nil
}
