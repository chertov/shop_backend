package kvdb

import (
	"errors"
	"context"
	"tools"
	"kinds"
)

type Session struct {
	Sid     string
	Order	interface{}
	User    interface{}
}

var sessions = make(map[string]Session)

func NewSession() (Session, error) {
	session := Session{}
	session.Sid = tools.RandStringRunes(10)
	sessions[session.Sid] = session
	return session, nil
}

func GetSession(sid string) (Session, bool, error) {
	session, ok:= sessions[sid]
	return session, ok, nil
}

func SaveSession(ses Session) error {
	sessions[ses.Sid] = ses

	if ses.User != nil && ses.Order != nil {
		order := kinds.Order{}
		order.FromTNT(ses.Order)
		user := kinds.User{}
		user.FromTNT(ses.User)
		SaveUserOrder(&user, &order)
	}
	return nil
}

func GetFromContext(ctx context.Context) (Session, bool, error) {
	ses_inf := ctx.Value("ses")
	if ses_inf == nil {
		return Session{}, false, errors.New("Context doesn't have session key 'ses'")
	}
	ses, ok := ses_inf.(Session)
	if !ok {
		return Session{}, false, errors.New("Context session can't cast key 'ses' to Session")
	}
	return ses, ok, nil
}