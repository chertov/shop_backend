package panic

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"runtime"
	"runtime/debug"
	"telegrambot"
	"strconv"
	"strings"
)

var render_full_stcak bool = false

type StackFunc struct {
	OneLine    bool   `json:"oneline"`
	StackLine1 string `json:"-"`
	StackLine2 string `json:"-"`
	File       string `json:"file"`
	Line       int64  `json:"line"`
	Pointer    string `json:"pointer"`
	Function   string `json:"function"`
	Code       string `json:"code"`
}
type Stack []StackFunc

func (sfunc StackFunc) ToJSON() string {
	b, _ := json.MarshalIndent(sfunc, "", "    ")
	return string(b)
}
func (stack Stack) ToJSON() string {
	b, _ := json.MarshalIndent(stack, "", "    ")
	return string(b)
}
func (stack Stack) Clear() Stack {
	newStack := make(Stack, 0)
	for _, stackFunc := range stack {
		if stackFunc.OneLine {
			continue
		}
		if stackFunc.Function == "PanicRecover" {
			continue
		}
		if stackFunc.Function == "panicmem" {
			continue
		}
		if stackFunc.Function == "gopanic" {
			continue
		}
		if stackFunc.Function == "goexit" {
			continue
		}
		if stackFunc.Function == "sigpanic" {
			continue
		}
		if stackFunc.Function == "call32" {
			continue
		}
		if stackFunc.Function == "main" && stackFunc.Code == "main_main()" {
			continue
		}
		log.Println(stackFunc.Function)
		newStack = append(newStack, stackFunc)
	}
	return newStack
}

func (stack Stack) LastMyCode() StackFunc {
	var fun StackFunc

	for i := len(stack) - 1; i >= 0; i-- {
		stackFunc := stack[i]
		if stackFunc.OneLine {
			i++
			return stack[i]
		}
	}
	return fun
}

func ParceStack(stack_str string) (Stack, error) {
	stack := make(Stack, 0)
	stack_lines := strings.Split(stack_str, "\n")

	i := 0
	for i < len(stack_lines) {
		var stackFunc StackFunc
		{
			line := strings.TrimSpace(stack_lines[i])
			if len(line) == 0 {
				i++
				continue
			}
			stackFunc.StackLine1 = line
			pos := strings.LastIndex(line, " ")
			if pos < 0 {
				log.Println("stack", stack_str)
				log.Println("problem line", len(line), line)
				err := errors.New("Не могу распарсить первую строку стека")
				return stack, err
			}
			file_line := line[0:pos]
			file_end := strings.LastIndex(file_line, ":")
			stackFunc.File = file_line[0:file_end]
			stackFunc.Line, _ = strconv.ParseInt(file_line[file_end+1:len(file_line)], 10, 64)
			stackFunc.Pointer = line[pos+1 : len(line)]
			i++
		}
		{
			line := strings.TrimSpace(stack_lines[i])
			stackFunc.StackLine2 = line
			pos := strings.Index(line, ": ")
			if pos < 0 {
				stackFunc.OneLine = true
				//log.Println("stack", stack_str)
				//log.Println("problem line", line)
				//err := errors.New("Не могу распарсить вторую строку стека")
				//return stack, err
				stack = append(stack, stackFunc)
				continue
			}
			stackFunc.OneLine = false
			stackFunc.Function = strings.TrimSpace(line[0:pos])
			stackFunc.Code = strings.TrimSpace(line[pos+2 : len(line)])
			i++
		}
		stack = append(stack, stackFunc)
	}
	//	if len(stack) < 2 {
	//		err := errors.New("Стек недостаточно большой")
	//		return stack, err
	//	}
	return stack, nil
}
func PanicRecover() {
	if err := recover(); err != nil {
		stack_str := string(debug.Stack())
		log.Println(stack_str)
		stack, stack_err := ParceStack(stack_str)
		if stack_err != nil {
			log.Println(stack_str)
			log.Println(stack_err)
		} else {
			//stack = stack.Clear()
			//stack = stack[3 : len(stack)-1]
			//log.Println(stack.ToJSON())
			//stack_func := stack[0]
			stack_func := stack.LastMyCode()
			str := "\nПроизошла критическая ошибка в функции " + stack_func.Function + "\n" +
				stack_func.File + ":" + strconv.FormatInt(stack_func.Line, 10) + "\n" +
				"Участок кода: " + stack_func.Code + "\n" +
				"Ошибка: " + fmt.Sprint(err)
			log.Println(str)
			telegrambot.Log(str)
		}
		os.Exit(3)
	}
}

var panicer bool = true

type MakePanic func(error) error

func Panic(err error) error {
	if panicer {
		stack_str := string(debug.Stack())
		stack, stack_err := ParceStack(stack_str)
		if stack_err != nil {
			log.Println(stack_str)
			log.Println(stack_err)
		} else {
			stack_func := stack[1]
			str := "\nПроизошла критическая ошибка в функции " + stack_func.Function + "\n" +
				stack_func.File + ":" + strconv.FormatInt(stack_func.Line, 10) + "\n" +
				"Участок кода: " + stack_func.Code + "\n" +
				"Ошибка: " + err.Error()
			log.Println(str)
			telegrambot.Log(str)
		}
		os.Exit(3)
		runtime.Goexit()
	}
	return err
}
