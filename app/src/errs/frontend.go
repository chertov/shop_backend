package errs

import (
	"encoding/json"
	"strconv"
	"errors"
)

func (err Error) ToFrontend() error {
	data := map[string]interface{}{"code": err.Code, "name": err.Name, "msg": err.Description}
	// data["trace"] = err.Trace
	b, jserr := json.MarshalIndent(data,"","    ")
	if jserr == nil {
		return errors.New(string(b))
	}
	return errors.New("{code:" + strconv.FormatInt(int64(err.Code),10) + ",name: '" + err.Name + "'}")
}