package tools

import (
	"time"
	"math/rand"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
var numbersRunes = []rune("0123456789")

func init() {
	rand.Seed(time.Now().UnixNano())
}

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
func RandNumberRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = numbersRunes[rand.Intn(len(numbersRunes))]
	}
	return string(b)
}
func NewPassword() string {
	return RandNumberRunes(5)
}