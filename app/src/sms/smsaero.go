package sms

import (
	"net/http"
	"log"
	"encoding/json"
	"io/ioutil"
	"kinds"
	"strconv"
	"strings"
)


type smsAero struct {
	loginUrl string
}

func newSMSAero() smsAero {
	smsAero := smsAero{}
	login := kinds.Config.SMSAeroLogin
	password := kinds.Config.SMSAeroAPIkey
	smsAero.loginUrl = "user=" + login + "&password=" + password
	return smsAero
}


type smsAeroBalance struct {
	Balance float64 	`json:",string,omitempty"`
}
func (r smsAero)GetBalance() (float64, error) {
	url := "https://gate.smsaero.ru/balance/?" + r.loginUrl + "&answer=json"

	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln("SMSAero GetBalance http.Get", err)
		return 0.0, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln("SMSAero GetBalance ioutil.ReadAll", err)
	}

	balance := smsAeroBalance{}
	err = json.Unmarshal(body, &balance)
	if err != nil {
		log.Fatalln("SMSAero GetBalance json.Unmarshal", err)
		return 0.0, err
	}
	return balance.Balance, nil
}
func (r smsAero)SendSMS(phone, msg string) error {
	if !kinds.Config.Sendsms {
		return nil
	}

	log.Println("kinds.Config", kinds.Config)
	loginUrl := "user=" + kinds.Config.SMSAeroLogin + "&password=" + kinds.Config.SMSAeroAPIkey
	phone = strings.Replace(phone, "+", "", -1)
	log.Println("loginUrl", loginUrl)
	url := "https://gate.smsaero.ru/send/?" + loginUrl +
		"&to=" + phone +
		"&text=" + msg +
		"&type=" + strconv.FormatInt(int64(kinds.Config.SMSAeroType), 10) +
		"&from=" + kinds.Config.SMSAeroName +
		"&answer=json"

	log.Println("sms url", url)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln("SMSAero SendSMS http.Get err", err)
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln("SMSAero SendSMS ioutil.ReadAll", err)
	}
	log.Println(string(body))

	return nil
}
