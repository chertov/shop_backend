package sms

type SMSProvider interface {
	SendSMS(phone, msg string) error
	GetBalance() (float64, error)
}

var SMS SMSProvider

func init() {
	SMS = newSMSAero()
}
