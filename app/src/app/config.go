package main

import (
	"runtime"
	"io/ioutil"
	"log"
	"path/filepath"
	"time"
	"os"
	"sort"
)

var yandexPathReleases = ""
func init() {
	if runtime.GOOS == "windows" {
		yandexPathReleases = "C:\\YandexDisk\\releases\\"
		yandexPathReleases = "./"
	} else {
		yandexPathReleases = "./"
	}
}

type pbfFile struct {
	File os.FileInfo
	ModTime time.Time
}
type pbfList []pbfFile

func (pbfs pbfList) Len() int           { return len(pbfs) }
func (pbfs pbfList) Swap(i, j int)      { pbfs[i], pbfs[j] = pbfs[j], pbfs[i] }
func (pbfs pbfList) Less(i, j int) bool { return pbfs[i].ModTime.Before(pbfs[j].ModTime) }

func GetItemsPBF() {

	switch os := runtime.GOOS; os {
	case "darwin":
		log.Println("OS X.")
		pbfs_path := "/Users/user/Yandex.Disk.localized/ProductEditor/"
		files, _ := ioutil.ReadDir(pbfs_path)
		pbfs := make(pbfList, 0)
		for _, f := range files {
			if filepath.Ext(f.Name()) == ".pbf" {
				pbfs = append(pbfs, pbfFile{f,f.ModTime()})
			}
		}
		sort.Sort(pbfList(pbfs))
		latest := pbfs[len(pbfs) - 1]
		log.Println(latest.File.Name())
		data, _ := ioutil.ReadFile(pbfs_path + latest.File.Name())
		ioutil.WriteFile("items.pbf", data, 0644)
		ioutil.WriteFile("/Users/user/Yandex.Disk.localized/releases/items.pbf", data, 0644)
	case "linux":
		log.Println("Linux.")
	case "windows":
		pbfs_path := "C:\\YandexDisk\\ProductEditor\\"
		files, _ := ioutil.ReadDir(pbfs_path)
		pbfs := make(pbfList, 0)
		for _, f := range files {
			if filepath.Ext(f.Name()) == ".pbf" {
				pbfs = append(pbfs, pbfFile{f,f.ModTime()})
			}
		}
		sort.Sort(pbfList(pbfs))
		latest := pbfs[len(pbfs) - 1]
		log.Println(latest.File.Name())
		data, _ := ioutil.ReadFile(pbfs_path + latest.File.Name())
		ioutil.WriteFile("items.pbf", data, 0644)
		ioutil.WriteFile("C:\\YandexDisk\\releases\\items.pbf", data, 0644)
	default:
		log.Printf("%s.", os)
	}
}