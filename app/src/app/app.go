package main

import (
	//"tnt"
	"log"
	"gql"
	// "encoding/json"
	"kvdb"
)

//go:generate go run ../err_gen.go

func main() {

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	err := kvdb.Init()
	if err != nil {
		log.Panicln(err)
	}
	kvdb.MonitorConfig()

	err = BotInit()
	if err != nil {
		log.Panicln(err)
	}

	GetItemsPBF()

	//err = tnt.Connect()
	//if err != nil {
	//	log.Panicln(err)
	//}
	//_, ok, err := tnt.Db.GetSession("a6ca04f3-508f-4ed7-b1f2-0249cc237343")
	//if err != nil {
	//	log.Panicln(err)
	//}
	//if !ok {
	//	log.Println("Session can't find")
	//}

	// gql.LoadPBF(yandexPathReleases+"items.pbf", yandexPathReleases+"prices.pbf")
	gql.LoadPBF(yandexPathReleases+"items.pbf", yandexPathReleases+"prices.yml")
	InitServer()

}
