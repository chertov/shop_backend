package main

import (
	"telegrambot"
	"kinds"
)

func BotInit() error {
	err := telegrambot.InitBot(kinds.Config.TelegramToken, false)
	if err != nil {
		return err
	}

	// telegrambot.SetChatId(76513882) // chertov
	// telegrambot.SetChatId(-38435682) // group Product Editor
	telegrambot.SetChatId(int64(kinds.Config.TelegramChatId)) // group Product Editor

	return nil
}

