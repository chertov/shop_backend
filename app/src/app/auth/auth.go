package auth

type AuthResolver struct {
}

func (r AuthResolver) Google() *GoogleAuthResolver {
	return &GoogleAuthResolver{}
}

type userResolver struct {
	id      string
	name    string
	email   string
}
func (r *userResolver) Id() string {
	return r.id
}
func (r *userResolver) Name() string {
	return r.name
}
func (r *userResolver) Email() string {
	return r.email
}