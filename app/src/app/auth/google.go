package auth

import (
	"encoding/json"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"io/ioutil"
	"log"
	"net/http"
	"context"
	"errors"
	"kvdb"
)

var googleConfig = &oauth2.Config{
	ClientID:     "206182104324-gkpl4nhe8k845vpbstf1u5khfpn11ces.apps.googleusercontent.com",
	ClientSecret: "Z7lzVxqe4ghpSp2Y1eo6cthf",
	// RedirectURL:  "https://brain-fuck.me/google_auth_redirect",
	RedirectURL:  "http://127.0.0.1:3010/google_auth_redirect",
	Scopes:       []string{"profile", "email", "openid"},
	Endpoint:     google.Endpoint,
}

type googleUser struct {
	Id          string `json:"id"`
	Email       string `json:"email"`
	Name        string `json:"name"`
}

type GoogleAuthResolver struct {
}
func (r *GoogleAuthResolver) LoginURL() string {
	return googleConfig.AuthCodeURL("")
}
func (r *GoogleAuthResolver) Auth(ctx context.Context, args *struct{Code string}) (*userResolver, error) {
	tok, err := googleConfig.Exchange(oauth2.NoContext, args.Code)
	if err != nil {
		log.Println("google_auth_redirect googleConfig.Exchange: ", err)
		return nil, err
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + tok.AccessToken)
	if err != nil {
		log.Println("google_auth_redirect http.Get: ", err)
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	// log.Println(string(body))

	user := googleUser{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Println("google_auth_redirect json.Unmarshal: ", err)
		return nil, err
	}

	ses, ok, err := kvdb.GetFromContext(ctx)
	if err != nil {
		log.Println("google_auth_redirect session.GetFromContext(ctx): ", err)
		return nil, err
	}
	if !ok {
		err := errors.New("google_auth_redirect session does not exist: ")
		log.Println(err)
		return nil, err
	}
	// ses.User = &tnt.UserData{Id:user.Id, Type:"user", Name:user.Name}
	kvdb.SaveSession(ses)

	return &userResolver{id:user.Id, name:user.Name, email:user.Email}, nil
}

