package main

import (
	"context"
	"github.com/gorilla/mux"
	"github.com/neelance/graphql-go"
	"github.com/neelance/graphql-go/relay"
	"io/ioutil"
	"net/http"
	"time"
	"path/filepath"
	"os"
	"log"
	"gql"
	"kvdb"
)

var schema *graphql.Schema

func InitGraphQL() {
	schema_path, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	schema_path = filepath.Join(schema_path, "./schema.graphqls")
	b, err := ioutil.ReadFile(schema_path)
	if err != nil {
		panic(err)
	}
	Schema := string(b)
	schema, err = graphql.ParseSchema(Schema, &gql.Resolver{})
	if err != nil {
		panic(err)
	}
}

func setNewSession(res http.ResponseWriter) kvdb.Session {
	ses, err := kvdb.NewSession()
	if err != nil {
		log.Panicln(err)
	}
	expire := time.Now().Add(5* 356 * 24 * time.Hour)
	cookie := http.Cookie{Name: "sid", Value: ses.Sid, Expires: expire}
	http.SetCookie(res, &cookie)
	return ses
}

func getSession(res http.ResponseWriter, req *http.Request) (kvdb.Session, bool) {
	url := "http://" + req.Host + req.URL.String()
	// получаем sid из кук
	cookie, _ := req.Cookie("sid")
	if cookie == nil {  // если нету,то устанавливаем и редиректим сюда же для получения
		log.Println("Can't find sid in cookies  ", url)
		return setNewSession(res), true
	}
	sid := cookie.Value
	if sid == "admin_cb9c1a5b" {
		session := kvdb.Session{"admin_cb9c1a5b", nil, nil}
		return session, true
	}

	ses, ok, err := kvdb.GetSession(sid)
	if err != nil {
		log.Panicln(err)
	}
	if !ok {   // если сессия не найдена,то устанавливаем её заново
		log.Println("Can't find session with sid=",sid,"   url:", url)
		return setNewSession(res), true
	}
	return ses, true
}

func authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {

		// url := "http://" + req.Host + req.URL.String()
		// log.Println("url:", url)
		ses, _ := getSession(res, req)
		// log.Println("ses:", ses)

		// start := time.Now()
		ctx := context.Background()
		ctx = context.WithValue(ctx, "ses", ses)
		req = req.WithContext(ctx)
		next.ServeHTTP(res, req)
		// log.Println("query ", time.Since(start))
	})
}

func logMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		//url := "http://" + req.Host + req.URL.String()
		//log.Println("log url:", url)
		next.ServeHTTP(res, req)
	})
}


func InitServer() {
	InitGraphQL()

	rootHandle := mux.NewRouter()
	rootHandle.Handle("/sid/{sid}", http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		sid := mux.Vars(req)["sid"]
		// запрашиваем сессию по sid
		_, ok, err := kvdb.GetSession(sid)
		if err!=nil {
			panic(err)
		}
		if !ok {   // если сессия не найдена,то устанавливаем её заново
			ses, _ := kvdb.NewSession()
			log.Println("SID Check: Can't find session with sid =",sid,"new sid is",ses.Sid)
			res.Write([]byte(ses.Sid))
			return
		}
		res.Write([]byte("ok"))
	}))

	rootHandle.Handle("/", authMiddleware(http.HandlerFunc(indexPage)))
	rootHandle.Handle("/query", authMiddleware(&relay.Handler{Schema: schema}))

	log.Fatal(http.ListenAndServe(":8080", logMiddleware(rootHandle)))
}

func indexPage(res http.ResponseWriter, req *http.Request) {
	html := `
	<!DOCTYPE html>
	<html>
	    <head>
	        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/graphiql/0.7.8/graphiql.css" />
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/fetch/1.0.0/fetch.min.js"></script>
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.3.2/react.min.js"></script>
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.3.2/react-dom.min.js"></script>
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/graphiql/0.7.8/graphiql.js"></script>
	    </head>
	    <body style="width: 100%; height: 100%; margin: 0; overflow: hidden;">
	        <div id="graphiql" style="height: 100vh;">Loading...</div>
	        <script>
	            function graphQLFetcher(graphQLParams) {
	                graphQLParams.variables = graphQLParams.variables ? JSON.parse(graphQLParams.variables) : null;
	                return fetch("/query", {
	                    method: "post",
	                    body: JSON.stringify(graphQLParams),
	                    credentials: "include",
	                }).then(function (response) {
	                    return response.text();
	                }).then(function (responseBody) {
	                    try {
	                        return JSON.parse(responseBody);
	                    } catch (error) {
	                        return responseBody;
	                    }
	                });
	            }
	            ReactDOM.render(
	                React.createElement(GraphiQL, {fetcher: graphQLFetcher}),
	                document.getElementById("graphiql")
	            );
	        </script>
	    </body>
	</html>
	`
	res.Write([]byte(html))
}
