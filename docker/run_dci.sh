cd .. && \
./build.sh .. && \
cp bin/server ./../isomorphic/bin/ && \
cp bin/brainfuck-2d73795b7960.json ./../isomorphic/bin/ && \
cp bin/schema.graphqls ./../isomorphic/bin/ && \
cp bin/items.pbf ./../isomorphic/bin/ && \
cp bin/prices.yml ./../isomorphic/bin/ && \
cd docker && \
docker-compose -f dc.yml -H tcp://127.0.0.1:2376 build && docker-compose -f dc.yml -H tcp://127.0.0.1:2376 up
