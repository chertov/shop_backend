cd .. && \
./build.sh .. && \
cp bin/server ./../isomorphic/bin/ && \
cp bin/brainfuck-2d73795b7960.json ./../isomorphic/bin/ && \
cp bin/schema.graphqls ./../isomorphic/bin/ && \
cp bin/items.pbf ./../isomorphic/bin/ && \
cp bin/prices.yml ./../isomorphic/bin/ && \
cp ./../isomorphic/pm2_process.yml ./../isomorphic/bin/ && \
cd docker && \
docker-compose build && docker-compose up
