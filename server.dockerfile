FROM golang:1.8-alpine
MAINTAINER Chertov Maxim <chertovmv@gmail.com>

RUN set -x \
    && apk add --no-cache --virtual .run-deps \
        git

RUN mkdir /app
RUN mkdir /app/bin

COPY ./app/ /app/app
COPY ./build.sh /app

WORKDIR /app
RUN chmod 777 ./build.sh
RUN ./build.sh

VOLUME /app/bin/
EXPOSE 3311

#CMD ["/bin/bash", "-l"]
ENTRYPOINT cp /app/bin/server /app/release/server
