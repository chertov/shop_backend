FROM alpine:3.4
MAINTAINER Chertov Maxim <chertovmv@gmail.com>

RUN apk add --no-cache \
        libstdc++ \
        readline \
        openssl \
        yaml \
        lz4 \
        binutils \
        ncurses \
        libgomp \
        lua \
        curl \
        tar \
        zip \
        perl \
        gcc \
        g++ \
        cmake \
        readline-dev \
        openssl-dev \
        yaml-dev \
        lz4-dev \
        binutils-dev \
        ncurses-dev \
        lua-dev \
        musl-dev \
        make \
        git

RUN apk add --no-cache --virtual nginx
RUN mkdir /var/cache/nginx

RUN apk add --no-cache pcre-dev libxml2-dev libxslt-dev gd-dev geoip-dev perl-dev

WORKDIR /root/
RUN git clone https://github.com/tarantool/nginx_upstream_module.git /root/nginx_upstream_module
WORKDIR /root/nginx_upstream_module
RUN git submodule update --init --recursive

RUN wget https://nginx.org/download/nginx-1.11.1.tar.gz
RUN tar -xvf nginx-1.11.1.tar.gz
RUN mv nginx-1.11.1 nginx
RUN make build-all
WORKDIR /root/nginx_upstream_module/nginx
RUN ./configure \
        --add-module=/root/nginx_upstream_module \
        --prefix=/etc/nginx \
        --sbin-path=/usr/sbin/nginx \
        --modules-path=/usr/lib/nginx/modules \
        --conf-path=/etc/nginx/nginx.conf \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --pid-path=/var/run/nginx.pid \
        --lock-path=/var/run/nginx.lock \
        --http-client-body-temp-path=/var/cache/nginx/client_temp \
        --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
        --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
        --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
        --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
        --with-http_ssl_module \
        --with-http_realip_module \
        --with-http_addition_module \
        --with-http_sub_module \
        --with-http_dav_module \
        --with-http_flv_module \
        --with-http_mp4_module \
        --with-http_gunzip_module \
        --with-http_gzip_static_module \
        --with-http_random_index_module \
        --with-http_secure_link_module \
        --with-http_stub_status_module \
        --with-http_auth_request_module \
        --with-http_xslt_module=dynamic \
        --with-http_image_filter_module=dynamic \
        --with-http_geoip_module=dynamic \
        --with-http_perl_module=dynamic \
        --with-threads \
        --with-stream \
        --with-stream_ssl_module \
        --with-http_slice_module \
        --with-mail \
        --with-mail_ssl_module \
        --with-http_v2_module \
        --with-ipv6

RUN make
RUN make install
RUN nginx -V

ADD ./conf/ /app/nginx/conf/
WORKDIR /app/nginx/
RUN mkdir /app/nginx/logs/
RUN cp /app/nginx/conf/nginx.conf /etc/nginx/nginx.conf
# RUN nginx -t

RUN mkdir /keys/
ADD ./.acme.sh /keys/.acme.sh

EXPOSE 80
EXPOSE 443

ENTRYPOINT nginx && bash
