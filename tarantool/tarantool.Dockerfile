FROM alpine:3.4
MAINTAINER Chertov Maxim <chertovmv@gmail.com>

RUN addgroup -S tarantool \
    && adduser -S -G tarantool tarantool \
    && apk add --no-cache 'su-exec>=0.2'

ENV TARANTOOL_VERSION=1.7.2-287-g085f6e4 \
    TARANTOOL_DOWNLOAD_URL=https://github.com/tarantool/tarantool.git \
    TARANTOOL_INSTALL_LUADIR=/usr/local/share/tarantool \
    LUAROCKS_URL=http://keplerproject.github.io/luarocks/releases/luarocks-2.3.0.tar.gz

RUN set -x \
    && apk add --no-cache --virtual .run-deps \
        libstdc++ \
        readline \
        openssl \
        yaml \
        lz4 \
        binutils \
        ncurses \
        libgomp \
        lua \
        curl \
        tar \
        zip \
    && apk add --no-cache --virtual .build-deps \
        perl \
        gcc \
        g++ \
        cmake \
        readline-dev \
        openssl-dev \
        yaml-dev \
        lz4-dev \
        binutils-dev \
        ncurses-dev \
        lua-dev \
        musl-dev \
        make \
        git

RUN set -x \
    && : "---------- tarantool ----------" \
    && mkdir -p /usr/src/tarantool \
    && git clone "$TARANTOOL_DOWNLOAD_URL" /usr/src/tarantool \
    && git -C /usr/src/tarantool checkout "$TARANTOOL_VERSION" \
    && git -C /usr/src/tarantool submodule update --init --recursive

RUN set -x \
    && (cd /usr/src/tarantool; \
       cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo\
             -DENABLE_BUNDLED_LIBYAML:BOOL=OFF\
             -DENABLE_BACKTRACE:BOOL=ON\
             -DENABLE_DIST:BOOL=ON\
             .) \
    && make -C /usr/src/tarantool -j\
    && make -C /usr/src/tarantool install \
    && make -C /usr/src/tarantool clean \
    && : "---------- small ----------" \
    && (cd /usr/src/tarantool/src/lib/small; \
        cmake -DCMAKE_INSTALL_PREFIX=/usr \
              -DCMAKE_INSTALL_LIBDIR=lib \
              -DCMAKE_BUILD_TYPE=RelWithDebInfo \
              .) \
    && make -C /usr/src/tarantool/src/lib/small \
    && make -C /usr/src/tarantool/src/lib/small install \
    && make -C /usr/src/tarantool/src/lib/small clean

RUN set -x \
    && : "---------- msgpuck ----------" \
    && (cd /usr/src/tarantool/src/lib/msgpuck; \
        cmake -DCMAKE_INSTALL_PREFIX=/usr \
              -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
              -DCMAKE_INSTALL_LIBDIR=lib \
              -DCMAKE_BUILD_TYPE=RelWithDebInfo \
              .) \
    && make -C /usr/src/tarantool/src/lib/msgpuck \
    && make -C /usr/src/tarantool/src/lib/msgpuck install \
    && make -C /usr/src/tarantool/src/lib/msgpuck clean \
    && : "---------- luarocks ----------" \
    && wget -O luarocks.tar.gz "$LUAROCKS_URL" \
    && mkdir -p /usr/src/luarocks \
    && tar -xzf luarocks.tar.gz -C /usr/src/luarocks --strip-components=1 \
    && (cd /usr/src/luarocks; \
        ./configure; \
        make build; \
        make install)

RUN set -x \
    && luarocks install https://raw.githubusercontent.com/tarantool/http/master/http-scm-1.rockspec --local

# RUN set -x \
#     && rm -r /usr/src/luarocks \
#     && rm -rf /usr/src/tarantool \
#     && : "---------- remove build deps ----------" \
#     && apk del .build-deps

RUN mkdir /app
RUN mkdir /app/tarantool
RUN mkdir /app/tarantool_db
RUN mkdir /app/tnt_src
RUN mkdir /app/tnt_lib
RUN mkdir /app/tnt_go
RUN mkdir -p /etc/tarantool/instances.enabled/
# VOLUME /var/log /var/db

COPY ./tarantool_lua/ /etc/tarantool/instances.enabled/
VOLUME /etc/tarantool/instances.enabled/
VOLUME /app/tarantool/


#COPY ./tarantool_c/ /app/tnt_src/
WORKDIR /app/tnt_src/


# RUN curl https://sh.rustup.rs -sSf
#RUN curl https://sh.rustup.rs > rust_install.sh
#RUN chmod 777 ./rust_install.sh
#RUN ./rust_install.sh
#RUN ls;exit 1

#RUN chmod 777 ./build.sh
#RUN ./build.sh

# RUN which tarantool
# RUN nm -D /usr/local/bin/tarantool
# RUN asdasdasd

EXPOSE 3311
EXPOSE 8080

#CMD ["/bin/bash", "-l"]
WORKDIR /etc/tarantool/instances.enabled/
ENTRYPOINT tarantool app.lua
