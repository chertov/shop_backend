#include <tarantool/module.h>
#include <msgpuck.h>
int harder(box_function_ctx_t *ctx, const char *args, const char *args_end)
{
    uint32_t arg_count = mp_decode_array(&args);
    printf("arg_count = %d\n", arg_count);
    uint32_t field_count = mp_decode_array(&args);
    printf("field_count = %d\n", field_count);
    uint32_t val;
    int i;
    for (i = 0; i < field_count; ++i) {
        val = mp_decode_uint(&args);
        printf("val=%d.\n", val);
    }
    return 0;
}
