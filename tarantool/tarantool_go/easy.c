#include <tarantool/module.h>
#include <msgpuck.h>

int easy(box_function_ctx_t *ctx, const char *args, const char *args_end)
{
    printf("hello world\n");

    {
        // data
        char tuple_buf[512];
        char *d = tuple_buf;
        d = mp_encode_array(d, 2);
        d = mp_encode_uint(d, 333);
        d = mp_encode_str(d, "hello", strlen("hello"));

        box_tuple_format_t *fmt = box_tuple_format_default();
        box_tuple_t *tuple = box_tuple_new(fmt, tuple_buf, d);
        if (tuple == NULL) return -1;
        if(box_return_tuple(ctx, tuple) == -1) return -1;
    }

    {
        // error
        char tuple_buf[512];
        char *d = tuple_buf;
        d = mp_encode_array(d, 2);
        d = mp_encode_uint(d, 777);
        d = mp_encode_str(d, "error", strlen("error"));

        box_tuple_format_t *fmt = box_tuple_format_default();
        box_tuple_t *tuple = box_tuple_new(fmt, tuple_buf, d);
        if (tuple == NULL) return -1;
        if(box_return_tuple(ctx, tuple) == -1) return -1;
    }
    return 0;
}
