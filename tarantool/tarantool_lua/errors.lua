
    function loadErrors()
        local yaml = require('yaml')
        local fio = require('fio')
        local errno = require('errno')

        local f = fio.open('/etc/tarantool/instances.enabled/errors.yml', {'O_RDONLY'})
        if not f then
            error("Failed to open file: "..errno.strerror())
            f:close()
            return nil
        end
        local data = f:read(1024*1024)
        f:close()
        return yaml.decode(data)
    end

    local yml_errors = loadErrors()
    local start_const = yml_errors.startcode    -- начальная константа с которой начинаются цифровые коды ошибок
    local count = 0
    for enum, error in pairs(yml_errors.errors) do  -- Копируем предустановленные коды
        yml_errors.errors[enum]['enum'] = enum
        yml_errors.errors[enum]['code'] = start_const + count
        count = count + 1
    end

    function GetErrors()
        return yml_errors, nil
    end

    local errors_defines = {}
    for enum, error in pairs(yml_errors.errors) do  -- Копируем предустановленные коды
        table.insert(errors_defines, error)
    end
    local errdef = function(code) return errors_defines[code - start_const + 1] end -- получить строковое значение ошибки по цифровому коду

    local Errors_mt = {}  -- Метатаблица для реализации создания ошибки через Errors(code, msg)
    Errors_mt.__index = Errors_mt
    function Errors_mt:__call(code, msg)
        if type(code) == nil and type(msg) == nil then  -- в случае отсутствия аргументов вызов Errors() работает как конструктор класса Errors
            return nil
        end
        -- if type(code) == nil and type(msg) == nil then  -- в случае отсутствия аргументов вызов Errors() работает как конструктор класса Errors
        --     local err = setmetatable({}, Errors)
        --     err.trace = debug.traceback('',3)           -- сохраняем стек при создании ошибки
        --     return err
        -- end
        local err = Errors:new()
        if type(code) == "number" then
            err.code = code
        end
        if type(code) == "string" then
            err.msg = code
            return err
            -- error(err, 2)
        end
        if type(msg) == "string" then
            err.msg = msg
        end
        err.enum = errdef(err.code).enum

        return err
    end

    Errors = {}
    Errors = setmetatable({}, Errors_mt)            -- Класс ошибки
    Errors.__index = Errors
    for i, err_define in ipairs(errors_defines) do  -- Копируем предустановленные коды
        Errors[err_define.enum] = err_define.code
    end
    Errors.code = Errors.INTERNAL_ERROR             -- Значения по умолчанию
    Errors.enum = errdef(Errors.code)
    Errors.msg = 'Internal error'

    function Errors:new()                           -- Конструктор ошибки
        local err = setmetatable({}, Errors)
        err.trace = debug.traceback('',3)           -- сохраняем стек при создании ошибки
        return err
    end

    function Errors:__tostring()                    -- строковое представление ошибки
        return 'Error ' .. errdef(self.code) .. ': ' .. self.msg .. self.trace
    end
