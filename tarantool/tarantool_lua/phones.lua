
    function InitPhones()
        --[[
            Информация о номерах телефонов пользователей.
            phones:
                number, user
        ]]
        f_phones_number = 1     -- номер телефона пользователя
        f_phones_user = 2       -- идентификатор пользователя

        phones = box.schema.space.create('phones', {if_not_exists = true})
        phones:create_index('phone', {type = 'hash', unique = true, parts = {f_phones_number, 'string'}, if_not_exists = true})
        phones:create_index('user', {type = 'hash', unique = true, parts = {f_phones_user, 'string'}, if_not_exists = true})
        phones:create_index('phone_user', {type = 'hash', unique = true, parts = {f_phones_number, 'string', f_phones_user, 'string'}, if_not_exists = true})
    end

    function rowToPhone(row)
        local phone = {}
        phone['Phone'] = row[f_phones_number]
        phone['User'] = row[f_phones_user]
        return phone
    end

    -- Проверка телефона:
    -- 1. Пользователь не авторизован, телефон зарегистрирован в бд.
    --    Сообщаем пользователю что телефонный номер уже зарегистрирован,просим авторизироваться.
    -- 2. Пользователь не авторизован, телефон не зарегистрирован в бд.
    --    Создаем нового пользователя, высылаем пароль на телефон. Оформляем заказ.

    -- 3. Пользователь авторизован, телефон зарегистрирован на другой аккаунт в бд.
    --    Сообщаем пользователю что телефонный номер уже зарегистрирован на другого пользователя,
    --    просим авторизироваться под другим номером, либо его изменить.
    -- 4. Пользователь авторизован, телефон зарегистрирован и принадлежит данному пользователю.
    --    Оформляем заказ.
    -- 5. Пользователь авторизован, телефон не зарегистрирован в базе.
    --    Оформляем заказ.
    function CheckPhone(ses, order)
        if type(order.Phone) ~= "string" then return nil, Errors(Errors.ORDER_PHONE_ISNT_CORRECT, "phone is not string") end

        -- Если указанный телефон принадлежит зарегистрированному пользователю,а он не авторизирован,то требуем авторизироваться
        local rowphone = phones:get(order.Phone)

        local password = nil
        if ses.User == nil then -- 1,2 Пользователь не авторизован
            if rowphone ~= nil then -- 1. Пользователь не авторизован, телефон зарегистрирован в бд.
                return nil, Errors(Errors.PHONE_EXISTS_USER_ISNOT_AUTH, 'The phone "' .. order.Phone .. '" exists, but user not auth')
            else -- 2. Пользователь не авторизован, телефон не зарегистрирован в бд.
                local phone, error = registrationPhone(order.Phone)
                if error ~= nil then return nil, error end
                users:update(phone.User, {{'=',f_users_name, order.Name},{'=',f_users_surname, order.Surname},{'=',f_users_patronymic, order.Patronymic}})
                return phone, nil
            end
        else -- 3,4,5 Пользователь авторизован
            if rowphone ~= nil then -- 3,4 телефон зарегистрирован
                if rowphone[f_phones_user] ~= ses.User.Id then -- 3. Пользователь авторизован, телефон зарегистрирован на другой аккаунт в бд.
                    return nil, Errors(Errors.PHONE_BELONGS_TO_ANOTHER_USER, 'The phone "' .. order.Phone .. '" belongs to another user')
                end
            end
        end

        -- 4. Пользователь авторизован, телефон зарегистрирован и принадлежит данному пользователю.
        -- 5. телефон не зарегистрирован
        return password, nil
    end

    function TryLoginByPhone(sid, phone, password)

        local ses, error = GetSession(sid)
        if error ~= nil then
            return nil, error
        end
        if ses.User ~= nil then return ses.User, nil end

        box.begin()
        -- получаем юзера по номеру телефона и паролю
        local user, error = authByPhone(phone, password)
        if error ~= nil then
            box.commit()
            return nil, error
        end

        -- перенос текущего заказа и sid из временной таблицы для гостей в таблице сессий авторизированных пользователей

        -- сохраняем сессию гостя и удаляем ее из временной таблицы
        local tempses = tempsessions:get(sid)
        logi('tempses', js(tempses))
        tempsessions:delete{sid}
        local order = tempses[f_tempsession_order]
        logi('order', js(order))
        logi('user', js(user))

        -- сохраняем в таблицу сессий авторизированных пользователей
        local userses = {}
        userses[f_session_id]=sid
        userses[f_session_user]=user.Id
        sessions:replace(userses)
        logi('userses', js(userses))

        if order ~= nil then
            -- сохраняем текущий заказ авторизированного пользователя
            -- TODO сделать объединение текущих заказов
            local userorder = {}
            userorder[f_usersorders_user]=user.Id
            userorder[f_usersorders_order]=order
            usersorders:replace(userorder)
        end

        box.commit()
        return user, error
    end

    function authByPhone(phone, password)
        -- получаем по номеру телефона идентификатор пользователя
        local row = phones:get{phone}
        if row == nil then return nil, Errors(Errors.PHONE_NOT_FOUND, 'phone "' .. phone .. '" not found') end
        local phone = rowToPhone(row)
        logi(js(row))

        -- проверяем по идентификатор пользователя его пароль
        local user, error = CheckPassword(phone.User, password)
        if error ~= nil then
            return nil, error
        end
        return user, nil
    end

    -- local result, error = RegistrationPhone('796291979141')
    -- logi('result', js(result))
    -- logi('error', js(error))
    -- logi('---------------')

    function registrationPhone(phone)
        -- TODO проверить валидность телефона

        local user, error = NewUser(phone)
        if error ~= nil then
            logi('Registration error', error)
            return nil, error
        end

        local row = {}
        row[f_phones_number] = phone
        row[f_phones_user] = user.Id
        local status, result = pcall(function() return phones:insert(row) end)
        if not status then
            local error = result
            logi('Registration error', error)
            return nil, error
        end

        local result = {}
        result['User'] = user.Id
        result['Phone'] = phone
        result['Password'] = user.Password
        return result, nil
    end
