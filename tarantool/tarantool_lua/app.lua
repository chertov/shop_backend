
local console = require('console')
package.cpath = package.cpath .. '/app/tnt_lib/?.so;'

ServerHandlers = {}
if testing ~= true then
    box.cfg {
        listen = "0.0.0.0:3311",
        --logger = 'app.log',
        --log_level=5,
        slab_alloc_arena = 0.1,
        work_dir = "/app/tarantool/",
    }
else
    box.cfg {
        listen = "127.0.0.1:3311",
        --logger = 'app.log',
        --log_level=5,
        slab_alloc_arena = 0.01,
        work_dir = "/app/tarantool/",
    }
end

src_path = '/etc/tarantool/instances.enabled/'
-- src_path = './'
dofile(src_path .. 'source.lua')
reload_files()

-- заупскаем при старте БД
function InitDB()
    InitTempSessions()
    InitUserSessions()
    InitUsers()
    InitPhones()
    InitOrders()

    -- logi('---------------')
    -- local phone, error = registrationPhone('79629197914')
    -- if error == nil then
    --     logi('phone', js(phone))
    --     logi('error', js(error))
    --     local user = users:get(phone.User)
    --     logi('user', js(user))
    --     users:update(phone.User, {{'=',f_users_name,'Максим'},{'=',f_users_surname,'Чертов'}})
    --     logi('---------------')
    -- end
end

InitDB()

box.once('schema', function()
    box.schema.user.grant('guest','read,write,execute','universe')
end)
-- box.schema.func.create('easy.easy', {language = 'C', if_not_exists = true})
-- box.schema.func.create('easy.harder', {language = 'C', if_not_exists = true})
-- box.schema.func.create('add.add', {language = 'C', if_not_exists = true})
-- box.schema.user.grant('guest', 'execute', 'function', 'easy.easy')
-- box.schema.user.grant('guest', 'execute', 'function', 'easy.harder')
-- box.schema.user.grant('guest', 'execute', 'function', 'add.add')

local net_box = require('net.box')
conn = net_box:new(3311)
-- conn:call('easy.easy')
-- conn:call('easy.harder')
-- conn:call('add')

StartHTTPAdmin()
logi('Server was started')

if testing ~= true then
    start_source_update()
    console.start()
end
