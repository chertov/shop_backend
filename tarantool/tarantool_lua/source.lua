local fiber = require('fiber')

local source_update_fiber_name = 'source_update_fiber'
local src_files = {'source.lua', 'tool.lua', 'errors.lua',
    'auth.lua',
    'sessions_temp.lua',
    'sessions_user.lua',
    'sessions.lua',
    'orders.lua',
    'phones.lua',
    'users.lua',
    'smsaero.lua',
    'admin/server.lua',
    'admin/users.lua',
    'admin/orders.lua'
}

function reload_files()
    for i, file in ipairs(src_files) do
        local full_path = src_path .. file
        local status, err = pcall(dofile, full_path)
        if not status then
            if not(string.starts(err, "cannot open ") and string.ends(err, "No such file or directory")) then
                print('    ', status, full_path)
                print('    ', "'" .. err .. "'")
            end
        end
    end
end

function start_source_update()
    stop_source_update()
    local source_update_func = function()
        while 0 == 0 do
            reload_files()
            fiber.sleep(1)
        end
    end
    local source_update_fiber = fiber.create(source_update_func)
    source_update_fiber:name(source_update_fiber_name)
end

function stop_source_update()
    for i, fiber_obj in pairs(fiber.info()) do
        if fiber_obj.name == source_update_fiber_name then
            fiber.kill(fiber_obj.fid)
        end
    end
end
