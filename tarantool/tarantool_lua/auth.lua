
function InitAuth()
    google = box.schema.space.create('google', {if_not_exists = true})
    google:create_index('id', {type = 'hash', unique = true, parts = {1, 'STR'}, if_not_exists = true})
    google:create_index('user', {type = 'hash', unique = true, parts = {2, 'STR'}, if_not_exists = true})
    google:create_index('email', {type = 'hash', unique = true, parts = {3, 'STR'}, if_not_exists = true})

    facebook = box.schema.space.create('facebook', {if_not_exists = true})
    facebook:create_index('id', {type = 'hash', unique = true, parts = {1, 'STR'}, if_not_exists = true})
    facebook:create_index('user', {type = 'hash', unique = true, parts = {2, 'STR'}, if_not_exists = true})
    facebook:create_index('email', {type = 'hash', unique = true, parts = {3, 'STR'}, if_not_exists = true})

    email = box.schema.space.create('email', {if_not_exists = true})
    email:create_index('email', {type = 'hash', unique = true, parts = {1, 'STR'}, if_not_exists = true})
    email:create_index('user', {type = 'hash', unique = true, parts = {2, 'STR'}, if_not_exists = true})
end

function GoogleLogin(google_id, email, access_token, name)
    box.begin()
    local user_id = ''
    local res = box.space.google:select{google_id}
    if table.getn(res) == 0 then
        user_id = uuid()
        box.space.users:insert{user_id, name}
        box.space.google:insert{google_id, user_id, email, access_token, name}
    else
        user_id = res[1][2]
    end
    box.commit()
    return user_id
end
