
function InitTempSessions()
    --[[
        Сессии для любых неавторизированных пользователей, одиночных заходов на сайт и т.д.
        могут быть очищены в любой момент.
        tempsessions:
            id, order
    ]]
    f_tempsession_id = 1        -- идентификатор сессии sid
    f_tempsession_order = 2     -- корзина неавторизированного пользователя

    tempsessions = box.schema.space.create('tempsessions', {if_not_exists = true})
    tempsessions:create_index('primary', {type = 'hash', unique = true, parts = {f_tempsession_id, 'STR'}, if_not_exists = true})
end

function getTempSession(sid)
    -- sid должен быть строковым идентификатором
    if type(sid) ~= "string" then return nil, Errors(Errors.SID_IS_NOT_STRING, "sid is not string") end

    local row = tempsessions:get(sid)
    if row == nil then return nil, nil end  -- сессия не найдена

    local session = {}
    session['Sid'] = sid

    local order, error = getTempSessionOrder(sid)
    if error ~= nil then
        return nil, error
    end
    session['Order'] = order

    return session, nil
end

function getTempSessionOrder(sid)
    if type(sid) ~= "string" then return nil, Errors(Errors.SID_IS_NOT_STRING, "sid is not string") end  -- sid должен быть строковым идентификатором

    local row = tempsessions:get(sid)
    if row == nil then return nil, nil end  -- сессия не найдена

    local order = row[f_tempsession_order]
    -- if order == nil then return nil, Errors(Errors.ORDER_ERROR, "order in temp session is nil") end
    return order, nil
end

function saveTempSession(ses)
    local row = {}
    row[f_tempsession_id] = ses.Sid
    row[f_tempsession_order] = ses.Order
    local status, tuple_err = pcall(function() return tempsessions:replace(row) end)
    if not status then -- если в процессе добавления произошла ошибка
        logi(tuple_err)
        return nil, Errors(Errors.ID_ALREADY_EXIST, "session already exist") -- возвращаем ошибку
    end

    return nil, nil
end
