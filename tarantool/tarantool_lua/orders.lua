
    local clock = require('clock')
    local os = require('os')

    function InitOrders()
        --[[
            Информация о заказах.
            orders
        ]]

        f_order_id = 1          -- идентификатор заказа
        f_order_user = 2        -- пользователь
        f_order_number = 3      -- номер заказа
        f_order_items = 4       -- данные заказа
        f_order_timeadd = 5     -- дата создания заказа
        f_order_timechange = 6  -- дата создания заказа
        f_order_state = 7       -- статус заказа
        f_order_messages = 8    -- сообщения заказа

        f_order_name = 9        -- имя получателя
        f_order_surname = 10    -- фамилия получателя
        f_order_patronymic = 11 -- отчество получателя
        f_order_phone = 12      -- контактный номер телефона получателя
        f_order_email = 13      -- контактный адрес электронной почты получателя
        f_order_comment = 14    -- комментарий

        f_order_iscompany = 15
        f_order_company = 16
        f_order_fizik = 17

        f_order_deliverytype = 18       -- тип доставки
        f_order_deliveryaddress = 19    -- адрес доставки

        orders = box.schema.space.create('orders', {if_not_exists = true})
        orders:create_index('id', {type = 'hash', unique = true, parts = {f_order_id, 'string'}, if_not_exists = true})
        orders:create_index('timeadd', {type = 'tree', unique = false, parts = {f_order_timeadd, 'number'}, if_not_exists = true})
        orders:create_index('timechange', {type = 'tree', unique = false, parts = {f_order_timechange, 'number'}, if_not_exists = true})
        orders:create_index('number', {type = 'hash', unique = true, parts = {f_order_number, 'number'}, if_not_exists = true})
        orders:create_index('user', {type = 'tree', unique = false, parts = {f_order_user, 'string'}, if_not_exists = true})
        orders:create_index('phone', {type = 'tree', unique = false, parts = {f_order_phone, 'string'}, if_not_exists = true})
        orders:create_index('email', {type = 'tree', unique = false, parts = {f_order_email, 'string'}, if_not_exists = true})
    end

    -- строка из orders в объект Order

    function rowToDelivery(row)
        local delivery = {}
        delivery['Type'] = row[f_order_deliverytype]
        delivery['Address'] = row[f_order_deliveryaddress]
        return delivery
    end
    function rowToOrder(row)
        local order = {}
        order['Id'] = row[f_order_id]
        order['User'] = row[f_order_user]
        order['Number'] = row[f_order_number]
        order['Items'] = row[f_order_items]
        order['TimeAdd'] = row[f_order_timeadd]
        order['TimeChange'] = row[f_order_timechange]
        order['State'] = row[f_order_state]
        order['Messages'] = row[f_order_messages]

        order['Name'] = row[f_order_name]
        order['Surname'] = row[f_order_surname]
        order['Patronymic'] = row[f_order_patronymic]
        order['Phone'] = row[f_order_phone]
        order['Email'] = row[f_order_email]
        order['Comment'] = row[f_order_comment]

        order['IsCompany'] = row[f_order_iscompany]
        order['Company'] = row[f_order_company]
        order['Fizik'] = row[f_order_fizik]

        order['Delivery'] = rowToDelivery(row)
        return order
    end

    -- количество заказов за последние сутки
    function CountOrdersInDay()
        local date = os.date("*t")

        local timestamp_start = os.time{year=date.year, month=1, day=date.yday, hour=0, min=0, sec=0}
        local timestamp_end = os.time{year=date.year, month=1, day=(date.yday+1), hour=0, min=0, sec=0}

        -- для теста..выборка заказов за текущую минуту
        -- timestamp_start = os.time{year=date.year, month=1, day=date.yday, hour=date.hour, min=date.min, sec=0}
        -- timestamp_end = os.time{year=date.year, month=1, day=date.yday, hour=date.hour, min=(date.min+1), sec=0}

        local orders_count = 0
        for _, tuple in orders.index.timeadd:pairs(timestamp_start, {iterator = 'GE', limit=10000}) do
            local time_in_sec = tuple[f_order_timeadd]
            if (time_in_sec >= timestamp_end) then break end
            orders_count = orders_count + 1
        end
        return orders_count
    end

    -- Генерируем номер заказа
    function NewOrderNumber()
        return tonumber(os.date('%y%m%d'):sub(2,6) .. ("%03d"):format(CountOrdersInDay()))
    end

    -- Создание нового заказа
    function MakeOrder(sid, order)
        if type(sid) ~= "string" then return nil, Errors(Errors.SID_IS_NOT_STRING, "sid is not string") end
        -- if type(order.Phone) ~= "number" then return nil, Errors(Errors.ORDER_PHONE_ISNT_CORRECT, "phone is not number") end

        local ses, error = GetSession(sid)
        if error ~= nil then return nil, error end
        if ses == nil then return nil, Errors(Errors.SESSION_NOT_FOUND, "session is not founded") end

        local phone, error = CheckPhone(sid, order)
        if error ~= nil then return nil, error end

        local orderrow = {}
        orderrow[f_order_id] = uuid()
        if ses.User ~= nil then
            orderrow[f_order_user] = ses.User.Id
        else
            orderrow[f_order_user] = phone.User
        end
        orderrow[f_order_number] = NewOrderNumber()
        orderrow[f_order_items] = order.Items
        orderrow[f_order_timeadd] = os.time()
        orderrow[f_order_timechange] = os.time()
        orderrow[f_order_state] = order.State
        orderrow[f_order_messages] = order.Messages

        orderrow[f_order_name] = order.Name
        orderrow[f_order_surname] = order.Surname
        orderrow[f_order_patronymic] = order.Patronymic
        orderrow[f_order_phone] = order.Phone
        orderrow[f_order_email] = order.Email
        orderrow[f_order_comment] = order.Comment

        orderrow[f_order_iscompany] = order.IsCompany
        orderrow[f_order_company] = order.Company
        orderrow[f_order_fizik] = order.Fizik

        orderrow[f_order_deliverytype] = order.Delivery.Type
        orderrow[f_order_deliveryaddress] = order.Delivery.Address
        local status, result = pcall(function() return orders:insert(orderrow) end)
        if not status then -- если в процессе добавления произошла ошибка
            local error = result
            logi('makeOrder error', error)
        end

        ses.Order = nil
        SaveSession(ses)

        local result = rowToOrder(orderrow)
        if phone ~= nil then result['NewUserPassword'] = phone.Password end
        return result, nil
    end

    function GetOrder(sid, orderId)
        if type(sid) ~= 'string' then return nil, Errors(Errors.SID_IS_NOT_STRING, "sid is not string") end
        if type(orderId) ~= 'string' then return nil, Errors(Errors.ORDERID_IS_NOT_STRING, "orderId is not string") end

        local ses, error = GetSession(sid)
        if error ~= nil then return nil, error end
        if ses == nil then return nil, Errors(Errors.SESSION_NOT_FOUND, "session is not founded") end
        if ses.User == nil then -- сессия гостевая
            local status, result = pcall(function() return orders:select(orderId) end)
            if not status then
                local error = result
                logi('getOrder err:', error)
                return nil, error
            end
            local row = result[1]
            return {rowToOrder(row)}, nil
        else
        end
        return nil, nil
    end

    -- orders:truncate()

    -- TODO удалить после отладки
    function ClearOrders()
        -- logi('clearOrders')
        -- local status, result = pcall(function() return orders:truncate() end)
        -- if not status then
        --     local error = result
        --     logi('clearOrders err:', error)
        --     return nil, error
        -- end
        -- logi('clearOrders', orders:len())
        return nil, nil
    end
    function GetOrders(sid)
        if type(sid) ~= "string" then return nil, Errors(Errors.SID_IS_NOT_STRING, "sid is not string") end

        local ses, error = GetSession(sid)
        if error ~= nil then return nil, error end
        if ses == nil then return nil, Errors(Errors.SESSION_NOT_FOUND, "session is not founded") end
        if ses.User ~= nil then
            local status, result = pcall(function() return orders.index.user:select(ses.User.Id) end)
            if not status then
                local error = result
                logi('getOrder err:', error)
                return nil, error
            end
            local orders = {}
            for index, row in pairs(result) do
                table.insert(orders, rowToOrder(row))
            end
            return orders, nil
        end
        return nil, nil
    end
