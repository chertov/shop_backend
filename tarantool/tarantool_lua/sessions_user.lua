
function InitUserSessions()
    --[[
        Сессии для любых авторизированных пользователей.
        Не желательно терять их состояние,т.к. это привидет к разлогиниванию их на клиентских устройствах.
        sessions:
            id, user, data
    ]]
    f_session_id = 1        -- идентификатор сессии sid
    f_session_user = 2      -- идентификатор пользователя сессии

    sessions = box.schema.space.create('sessions', {if_not_exists = true})
    sessions:create_index('primary', {type = 'hash', unique = true, parts = {f_session_id, 'STR'}, if_not_exists = true})
    sessions:create_index('user', {type = 'tree', unique = false, parts = {f_session_user, 'STR'}, if_not_exists = true})

    --[[
        Информация о текущих заказах пользователей.
        usersorders:
            user, order
    ]]
    f_usersorders_user = 1      -- идентификатор пользователя
    f_usersorders_order = 2     -- корзина

    usersorders = box.schema.space.create('usersorders', {if_not_exists = true})
    usersorders:create_index('user', {type = 'hash', unique = true, parts = {f_usersorders_user, 'string'}, if_not_exists = true})
end

function getUserSession(sid)
    -- sid должен быть строковым идентификатором
    if type(sid) ~= "string" then return nil, Errors(Errors.SID_IS_NOT_STRING, "sid is not string") end

    local row = sessions:get(sid)
    if row == nil then return nil, nil end   -- сессия не найдена

    local session = {}
    session['Sid'] = sid

    local row = users:get(row[f_session_user])
    if row == nil then return nil, nil end   -- сессия не найдена
    session['User'] = rowToUser(row)

    local order, error = getUserSessionOrder(sid)
    if error ~= nil then
        return nil, error
    end
    session['Order'] = order
    return session, nil
end

function getUserSessionOrder(sid)
    if type(sid) ~= "string" then return nil, Errors(Errors.SID_IS_NOT_STRING, "sid is not string") end  -- sid должен быть строковым идентификатором

    local row = sessions:get(sid)
    if row == nil then return nil, nil end  -- сессия не найдена
    local user_id = row[f_session_user]

    local row = usersorders:get(user_id)
    if row == nil then return nil, nil end  -- если текущий заказ не найден

    local order = row[f_usersorders_order]
    -- if order == nil then return nil, Errors(Errors.ORDER_ERROR, "order in user session is nil") end
    return order, nil
end

function saveUserOrder(ses)
    local row = {}
    row[f_usersorders_user] = ses.User.Id
    row[f_usersorders_order] = ses.Order
    local status, result = pcall(function() return usersorders:replace(row) end)
    if not status then -- если в процессе добавления произошла ошибка
        logi('saveUserOrder error', result)
        return nil, result
    end
    return nil, nil
end


function Logout(sid)
    -- sid должен быть строковым идентификатором
    if type(sid) ~= "string" then return nil, Errors(Errors.SID_IS_NOT_STRING, "sid is not string") end

    box.begin()
    sessions:delete{sid}
    local row = {}
    row[f_tempsession_id] = sid
    row[f_tempsession_order] = nil
    local status, tuple_err = pcall(function() return tempsessions:insert(row) end)
    if not status then -- если в процессе добавления произошла ошибка
        return nil, error
    end
    box.commit()

    local tempsession = {}
    tempsession['Sid'] = row[f_tempsession_id]
    return tempsession, nil
end
