
function NewSession()
    local row = {}
    row[f_tempsession_id] = uuid()
    row[f_tempsession_order] = nil
    local status, tuple_err = pcall(function() return tempsessions:insert(row) end)
    if not status then -- если в процессе добавления произошла ошибка
        return nil, Errors(Errors.ID_ALREADY_EXIST, "session already exist") -- возвращаем ошибку
    end

    local tempsession = {}
    tempsession['sid'] = row[f_tempsession_id]
    return tempsession, nil
end

function GetSession(sid)
    local session, error = getTempSession(sid)
    if session ~= nil then
        return session, nil
    end
    local session, error = getUserSession(sid)
    if session ~= nil then
        return session, nil
    end
    return nil, nil
end

function SaveSession(ses)
    if ses == nil then return nil, Errors(Errors.SESSION_IS_NOT_VALID, "session is not valid") end
    if type(ses.Sid) ~= "string" then return nil, Errors(Errors.SID_IS_NOT_STRING, "session sid is not string") end  -- sid должен быть строковым идентификатором

    local saved_ses, error = GetSession(ses.Sid)
    if session ~= nil then
        return nil, nil
    end

    if saved_ses.User == nil then -- сессия гостевая
        local res, error = saveTempSession(ses)
        if error ~= nil then return nil, error end
    else
        local res, error = saveUserOrder(ses)
        if error ~= nil then return nil, error end
    end
    return nil, nil
end
