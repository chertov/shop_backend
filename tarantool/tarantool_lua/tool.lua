
    local log = require('log')
    local os = require('os')
    local math = require('math')
    local json = require('json')

    function logi(...)
        -- log.info(table.concat({...},'\t'))
        local arguments = {...}
        local printResult = ''
        for i, v in ipairs(arguments) do
            printResult = printResult .. tostring(v) .. "\t"
        end
        log.info(printResult)
        print(printResult)
    end

    function js(obj)
        return json.encode(obj)
    end

    math.randomseed(os.time())
    function uuid()
        local template ='xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
        return string.gsub(template, '[xy]', function (c)
            local v = (c == 'x') and math.random(0, 0xf) or math.random(8, 0xb)
            return string.format('%x', v)
        end)
    end

    local passwordSymbols ='123456789ABDEFGHJKLMNPQRSTUWXYZabdefghjkmnpqrstuwxyz'
    local passwordSymbolsLen = string.len(passwordSymbols)
    function generatePassword()
        local pass =''
        for i=0, 5, 1 do
            local rnd = math.random(1, passwordSymbolsLen)
            pass = pass .. string.sub(passwordSymbols, rnd, rnd)
        end
        return pass
    end

    function string.split(source, delimiters)
        local elements = {}
        local pattern = '([^'..delimiters..']+)'
        string.gsub(source, pattern, function(value) elements[#elements + 1] =     value;  end);
        return elements
    end
    function string.starts(String, Start)
       return string.sub(String,1,string.len(Start))==Start
    end
    function string.ends(String, End)
       return End=='' or string.sub(String,-string.len(End))==End
    end

    -- Remove key k (and its value) from table t. Return a new (modified) table.
    -- local t = {name="swfoo", domain="swfoo.com"}
    -- t = table.removeKey(t, "domain")
    function table.removeKey(t, k)
    	local i = 0
    	local keys, values = {},{}
    	for k,v in pairs(t) do
    		i = i + 1
    		keys[i] = k
    		values[i] = v
    	end

    	while i>0 do
    		if keys[i] == k then
    			table.remove(keys, i)
    			table.remove(values, i)
    			break
    		end
    		i = i - 1
    	end

    	local a = {}
    	for i = 1,#keys do
    		a[keys[i]] = values[i]
    	end

    	return a
    end

    function table.removeDuplicate(t)
        local hash = {}
        local res = {}
        for _, v in ipairs(t) do
            if (not hash[v]) then
                res[#res+1] = v -- you could print here instead of saving to result table if you wanted
                hash[v] = true
            end
        end
        return res
    end

    -- Determine with a Lua table can be treated as an array.
    -- Explicitly returns "not an array" for very sparse arrays.
    -- Returns:
    -- -1   Not an array
    -- 0    Empty table
    -- >0   Highest index in the array
    local function is_array(table)
        if type(table) ~= 'table' then return -1 end
        local max = 0
        local count = 0
        for k, v in pairs(table) do
            if type(k) == 'number' then
                if k > max then max = k end
                count = count + 1
            else
                return -1
            end
        end
        if max > count * 2 then
            return -1
        end

        return max
    end

    local begin_opened = 0
    function begin()
        if begin_opened == 0 then box.begin() end
        begin_opened = begin_opened + 1
    end
    function commit()
        begin_opened = begin_opened - 1
        if begin_opened == 0 then box.commit() end
    end
    function rollback()
        begin_opened = begin_opened - 1
        if begin_opened == 0 then box.rollback() end
    end
