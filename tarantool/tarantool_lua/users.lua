
    local os = require('os')
    local digest = require('digest')

    function InitUsers()
        --[[
            Информация о пользователях.
            users:
                id, password
        ]]
        f_users_id = 1          -- идентификатор пользователя
        f_users_time = 2        -- дата создания пользователя
        f_users_password = 3    -- пароль пользователя для авторизации через email или phone
        f_users_name = 4        -- имя
        f_users_surname = 5     -- фамилия
        f_users_patronymic = 6  -- отчество

        users = box.schema.space.create('users', {if_not_exists = true})
        users:create_index('primary', {type = 'hash', unique = true, parts = {f_users_id, 'string'}, if_not_exists = true})
    end

    -- строка из orders в объект Order
    function rowToUser(row)
        local user = {}
        user['Id'] = row[f_users_id]
        user['Time'] = row[f_users_time]
        user['Name'] = row[f_users_name]
        user['Surname'] = row[f_users_surname]
        user['Patronymic'] = row[f_users_patronymic]
        return user
    end

    function NewUser(passwordSalt)
        local row = {}
        row[f_users_id] = uuid()
        row[f_users_time] = os.time()

        local Password = generatePassword()
        row[f_users_password] = digest.md5_hex(passwordSalt .. Password)

        local status, result = pcall(function() return users:insert(row) end)
        if not status then -- если в процессе добавления произошла ошибка
            local error = result
            logi('NewUser error', error)
            return nil, error
        end
        result = rowToUser(result)
        result['Password'] = Password
        return result, nil
    end

    function CheckPassword(id, password)
        local row = users:get{id}
        if row == nil then return nil, Errors(Errors.USER_NOT_FOUND, 'user with id "' .. id .. '" not found') end
        if row[f_users_password] == password then
            return rowToUser(row), nil
        end
        return nil, Errors(Errors.USER_PASSWORD_INCORRECT, 'user with id "' .. id .. '" password incorrect')
    end
