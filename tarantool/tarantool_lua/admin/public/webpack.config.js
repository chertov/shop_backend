
    const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
    const webpack = require('webpack');
    const path = require('path');
    const WebpackNotifierPlugin = require('webpack-notifier');
    const CircularDependencyPlugin = require('circular-dependency-plugin');
    const ProgressBarPlugin = require('progress-bar-webpack-plugin');
    const ExtractTextPlugin = require('extract-text-webpack-plugin');
    const combineLoaders = require('webpack-combine-loaders');

    const env = process.env.NODE_ENV || 'development';

    const browser = {
        name: 'Browser',
        entry: './jsx/app.jsx',
        output: {
            path: './js',
            filename: 'bundle.js'
        },

        stats: { children: false }, // prevent spam-logs in console
        watch: true,
        cache: true,
        debug: true,
        devtool: 'source-map',
        resolve: {
            root: [path.resolve(__dirname, 'jsx'), path.resolve(__dirname, 'node_modules')],
            extensions: ['', '.js', '.jsx', 'json']
        },
        module: {
            loaders: [
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
                },
                {
                    test: /\.less$/,
                    loader: ExtractTextPlugin.extract('style-loader',
                        combineLoaders([
                            { loader: 'css-loader', query: { sourceMap: true, importLoaders: 1, minimize: false } },
                            // { loader: 'csso-loader', query: { sourceMap: true } },
                            { loader: 'postcss-loader', query: { sourceMap: true } },
                            { loader: 'less-loader', query: { sourceMap: true } }
                        ])
                    )
                },
                { test: /\.(js|jsx)$/, exclude: /node_modules/, loader: 'babel-loader' },
                { test: /\.json$/, loader: 'json-loader' }
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify(env),
                }
            }),
            new BundleAnalyzerPlugin(),
            new ExtractTextPlugin('./../css/style.css'),
            // new webpack.optimize.DedupePlugin(),
            // new webpack.optimize.UglifyJsPlugin(),
            new WebpackNotifierPlugin({ excludeWarnings: true, alwaysNotify: true }),
            new ProgressBarPlugin(),
            new CircularDependencyPlugin({
                // add errors to webpack instead of warnings
                failOnError: true
            })
        ]
    };

    module.exports = [browser];
