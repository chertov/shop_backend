
    // Точка входа для рендера на стороне клиента

    import React from 'react';
    import ReactDOM from 'react-dom';
    import { Provider } from 'react-redux';

    import { Button } from 'semantic-ui-react';

    // import ClientProvider from 'clientprovider.jsx';
    // import getStore from './store';
    // import router from './router.jsx';

    /* global document */
    document.addEventListener('DOMContentLoaded', event => {
        // - Code to execute when all DOM content is loaded.
        // - including fonts, images, etc.
        // console.log('On server rendering: ', onserver());

        // fetch('/tree.json')
        // .then(response => response.json())
        // .then(tree => {
        //     if (exists(treeHash)) {
        //         const loadedTreeHash = getHash(JSON.stringify(tree));
        //
        //         if (loadedTreeHash !== treeHash) {
        //             console.log('reload without cache');
        //             const headers = new Headers();
        //
        //             headers.append('pragma', 'no-cache');
        //             headers.append('cache-control', 'no-cache');
        //
        //             return fetch(new Request('/tree.json'), { method: 'GET', headers })
        //             .then(response => response.json());
        //         }
        //     }
        //     return tree;
        // })
        // .then(tree => {
        //     const store = getStore({ tree });
        //
        //     console.log('state', store.getState());
        //
        // });

        ReactDOM.render(
            <Button>
                Click Here
            </Button>,
            document.getElementById('root')
        );
    });
