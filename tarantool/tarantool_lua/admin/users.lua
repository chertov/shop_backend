
    ServerHandlers['my_handler'] = function(self)
        -- -- req is a Request object
        -- local resp = req:render({text = req.method..' '..req.path })
        -- -- resp is a Response object
        -- resp.headers['x-test-header'] = 'test';
        -- resp.status = 201
        -- return resp

        -- logi('   ', req)

        local rows = users:select{}
        local allUsers = {}
        for i, row in ipairs(rows) do
            table.insert(allUsers, rowToUser(row))
        end

        return self:render({title = 'users', allUsers = allUsers, users = js(allUsers)})

        -- return {
        --     status = 200,
        --     headers = { ['content-type'] = 'text/html; charset=utf8' },
        --     body = [[
        --         <html>
        --             <body>Hello, world!</body>
        --         </html>
        --     ]]
        -- }
    end
