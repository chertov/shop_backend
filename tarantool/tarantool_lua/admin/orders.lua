
    ServerHandlers['orders'] = function(self)
        local rows = orders:select{}
        local all = {}
        for i, row in ipairs(rows) do
            table.insert(all, rowToOrder(row))
        end

        return self:render({title = 'Orders', orders = all, ordersJson = js(all)})
    end
