
    -- Обработчик GET-запросов к /
    local function handler(self)
        -- Получаем IP-адрес клиента
        -- local ipaddr = self.peer.host
        return self:render({ user = 'user'  })

	    -- -- req is a Request object
	    -- local resp = req:render({text = req.method..' '..req.path })
	    -- -- resp is a Response object
	    -- resp.headers['x-test-header'] = 'test';
	    -- resp.status = 201
	    -- return resp
    end


    function StartHTTPAdmin()
        local httpd = require('http.server')
        server = httpd.new('0.0.0.0', 8000, {
            app_dir = '/etc/tarantool/instances.enabled/admin',
            cache_templates     = false,
            cache_controllers   = false,
            cache_static        = false
        })
        server:start()
    end

    if server then
        server.routes = {}
        local httpd = require('http.server')
        server:route({ path = '/', template = 'Hello'  }, ServerHandlers.my_handler)
        server:route({ path = '/users', file = 'users.html'  }, ServerHandlers.my_handler)
        server:route({ path = '/orders', file = 'orders.html'  }, ServerHandlers.orders)
    end
