﻿@echo off
set GOROOT=c:\go
set GOARCH=amd64
set GOOS=windows
set ROOT_PATH=%~dp0
set GOPATH=%ROOT_PATH%;%ROOT_PATH%app\
set MODULE_PATH=%ROOT_PATH%app\src\app\
set TARGET_PATH=%ROOT_PATH%bin\
set PKG_PATH=%ROOT_PATH%pkg\
echo GOROOT: %GOROOT%
echo GOPATH: %GOPATH%
echo ROOT_PATH: %ROOT_PATH%
echo MODULE_PATH: %MODULE_PATH%
echo PKG_PATH: %PKG_PATH%
echo TARGET_PATH: %TARGET_PATH%
echo.
cd %MODULE_PATH%

REM Скачиваем пакеты,но не собираем
@echo on
go get -d ./
@echo off
echo.

@echo on
set GOOS=windows
go generate app.go
@echo off

:: REM Собираем для windows 64
:: set GOOS=windows
:: set TARGET=%TARGET_PATH%server.exe
:: echo Build for windows...
:: call:build_app

REM Собираем для linux 64
set GOOS=linux
set TARGET=%TARGET_PATH%server
echo Build for linux...
call:build_app

REM возврат в корневую директорию проекта и выход из скрипта
cd %ROOT_PATH%
goto:eof

REM функуция сборки приложения
:build_app
    echo GOOS: %GOOS%
    echo GOARCH: %GOARCH%
    echo TARGET: %TARGET%
    del %TARGET%
    @echo on
    go build -i -pkgdir %PKG_PATH% -o %TARGET%
    @echo off
    :: %TARGET_PATH%
goto:eof
